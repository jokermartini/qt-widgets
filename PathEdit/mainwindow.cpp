#include "mainwindow.h"
#include "pathedit.h"
#include "patheditbrowser.h"
#include <QVBoxLayout>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
{
    auto * pathEdit = new PathEdit();

    // Select File
    QStringList filters;
    filters << "Image files (*.png *.xpm *.jpg)"
            << "Text files (*.txt)"
            << "Any files (*)";
    auto * pathEditBrowserA = new PathEditBrowser();
    pathEditBrowserA->m_fileDialog->setNameFilters(filters);
    pathEditBrowserA->m_fileDialog->setAcceptMode(QFileDialog::AcceptOpen);
    pathEditBrowserA->m_fileDialog->setFileMode(QFileDialog::ExistingFile);

    // Select Folder
    auto * pathEditBrowserB = new PathEditBrowser();
    pathEditBrowserB->m_fileDialog->setAcceptMode(QFileDialog::AcceptOpen);
    pathEditBrowserB->m_fileDialog->setFileMode(QFileDialog::Directory);

    // Save File
    auto * pathEditBrowserC = new PathEditBrowser();
    pathEditBrowserC->m_fileDialog->setNameFilters(QStringList() << "*.vx" << "*.xml");
    pathEditBrowserC->m_fileDialog->setAcceptMode(QFileDialog::AcceptSave);
    pathEditBrowserC->m_fileDialog->setFileMode(QFileDialog::AnyFile);

    QWidget * widget = new QWidget();
    QVBoxLayout * layout = new QVBoxLayout(widget);
    layout->addWidget(pathEdit);
    layout->addWidget(pathEditBrowserA);
    layout->addWidget(pathEditBrowserB);
    layout->addWidget(pathEditBrowserC);
    setCentralWidget(widget);

}

MainWindow::~MainWindow()
{

}
