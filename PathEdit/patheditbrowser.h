#ifndef PATHEDITBROWSER_H
#define PATHEDITBROWSER_H

#include <QWidget>
#include "pathedit.h"
#include <QFileDialog>
#include <QPushButton>
#include <QHBoxLayout>

class PathEditBrowser : public QWidget
{
    Q_OBJECT

    Q_PROPERTY(QString text READ text WRITE setText RESET clear NOTIFY textChanged)

public:
    explicit PathEditBrowser(QWidget *parent = nullptr);

    //! Controls
    PathEdit *m_uiPathEdit;
    QPushButton *m_uiButton;
    QFileDialog *m_fileDialog;
    QHBoxLayout *layout;

    //! Read/Write Accessor
    QString text() const;
    bool setText(QString string);
    void clear();

signals:
    void textChanged(QString path);

public slots:
    void slotShowFileDialog();
    void slotFileSelected(const QString &string);
    void slotTextChanged(const QString &string);
};

#endif // PATHEDITBROWSER_H
