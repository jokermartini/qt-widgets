#ifndef PATHEDIT_H
#define PATHEDIT_H

#include <QWidget>
#include <QLineEdit>
#include <QAction>
#include <QCompleter>
#include <QFileSystemModel>

class PathEdit : public QLineEdit
{
    Q_OBJECT

public:
    explicit PathEdit(QWidget *parent = nullptr);

signals:

public slots:
  void slotCopyAll();
  void slotShowInExplorer();
  void slotTextChanged();

private:
    QAction *m_copyAllAct;
    QAction *m_showInExplorerAct;
    QCompleter *m_completer;
    QFileSystemModel *m_fileSystemModel;
    QString getIncremetedVersionString(const QString &str, int direction = 0);

protected:
    void contextMenuEvent(QContextMenuEvent *event) override;
    void dragEnterEvent(QDragEnterEvent *event) override;
    void dropEvent(QDropEvent *event) override;

protected:
    void keyPressEvent(QKeyEvent *event) override;
};

#endif // PATHEDIT_H
