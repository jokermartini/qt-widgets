#include "pathedit.h"

#include <QContextMenuEvent>
#include <QMenu>
#include <QClipboard>
#include <QApplication>
#include <QFileInfo>
#include <QProcess>
#include <QDir>
#include <QKeySequence>
#include <QDragEnterEvent>
#include <QMimeData>
#include <QString>
#include <QRegularExpression>
#include <QDebug>

PathEdit::PathEdit(QWidget *parent) :
    QLineEdit(parent)
{
    setClearButtonEnabled(true);
    setAcceptDrops(true);

    // Completer
    m_completer = new QCompleter();
    m_fileSystemModel = new QFileSystemModel(m_completer);
    m_fileSystemModel->setRootPath("");
    m_completer->setCompletionMode(QCompleter::UnfilteredPopupCompletion);
    m_completer->setCaseSensitivity(Qt::CaseInsensitive);
    m_completer->setMaxVisibleItems(10);
    m_completer->setWrapAround(true);
    m_completer->setModel(m_fileSystemModel);
    setCompleter(m_completer);

    // Create Actions
    m_copyAllAct = new QAction(tr("Copy All"), this);
    m_copyAllAct->setShortcutVisibleInContextMenu(true);

    m_showInExplorerAct = new QAction(tr("Show In Explorer"), this);
    m_showInExplorerAct->setShortcutVisibleInContextMenu(true);

    // Signals
    connect(m_copyAllAct, SIGNAL(triggered(bool)), this, SLOT(slotCopyAll()));
    connect(m_showInExplorerAct, SIGNAL(triggered(bool)), this, SLOT(slotShowInExplorer()));
    connect(this, SIGNAL(textChanged(QString)), this, SLOT(slotTextChanged()));
}

void PathEdit::slotCopyAll()
{
    QClipboard *clip = QApplication::clipboard();
    clip->setText(text());
}

void PathEdit::slotShowInExplorer()
{
    const QFileInfo file(text());

    if (!file.exists()) return;

    if (file.isFile()) {
        QStringList args;
        args << "/select," << QDir::toNativeSeparators(file.canonicalFilePath());
        QProcess *process = new QProcess(this);
        process->start("explorer.exe", args);
        return;
    }

    if (file.isDir()) {
        QStringList args;
        args << QDir::toNativeSeparators(file.canonicalFilePath());
        QProcess *process = new QProcess(this);
        process->start("explorer.exe", args);
        return;
    }
}

void PathEdit::slotTextChanged()
{
    setToolTip(text());
}

QString PathEdit::getIncremetedVersionString(const QString &str, int direction)
{
    QRegularExpression re("(v([0-9]+))");
    auto match = re.match(str);
    if (not match.hasMatch()) {
      return str;
    }

    auto part = match.captured(1); // returns v001
    part.remove(0,1); // removes 'v'

    auto padding = part.count();
    auto number = part.toInt() + direction;
    if (number < 0) {
        number = 0;
    }
    auto newPart = QString("v%1").arg(number, padding, 10, QChar('0'));

    QString result(str);
    return result.replace(re, newPart);
}

void PathEdit::contextMenuEvent(QContextMenuEvent *event)
{
    m_copyAllAct->setEnabled( !text().isEmpty() );
    m_showInExplorerAct->setEnabled( !text().isEmpty() );

    QMenu *menu = QLineEdit::createStandardContextMenu();
    menu->addAction(m_copyAllAct);
    menu->addSeparator();
    menu->addAction(m_showInExplorerAct);
    menu->exec((event->globalPos()));
    event->accept();
    delete menu;
}

void PathEdit::dragEnterEvent(QDragEnterEvent *event)
{
    const QMimeData *mimeData = event->mimeData();
    if (mimeData->hasUrls() && mimeData->urls().count()==1) {
        event->accept();
    } else {
        event->ignore();
    }
}

void PathEdit::dropEvent(QDropEvent *event)
{
    const QMimeData *mimeData = event->mimeData();
    if (mimeData->hasUrls()) {
        setText(mimeData->urls().first().toLocalFile());
    }
    event->accept();
}

void PathEdit::keyPressEvent(QKeyEvent *event)
{
    if ((event->key() == Qt::Key_Down) &&
            (QApplication::keyboardModifiers() & Qt::AltModifier))
    {
        this->setText(getIncremetedVersionString(this->text(), -1));
        return;
    }
    else if ((event->key() == Qt::Key_Up) &&
            (QApplication::keyboardModifiers() & Qt::AltModifier))
    {
        this->setText(getIncremetedVersionString(this->text(), 1));
        return;
    }

    QLineEdit::keyPressEvent(event);
}
