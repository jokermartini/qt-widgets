#include "patheditbrowser.h"
#include <QStandardPaths>
#include <QFileInfo>

PathEditBrowser::PathEditBrowser(QWidget *parent) :
    QWidget(parent)
{
    m_fileDialog = (new QFileDialog(this)),

    m_uiPathEdit = new PathEdit(this);
    m_uiButton = new QPushButton("...", this);
    m_uiButton->setFixedWidth(30);

    layout = new QHBoxLayout(this);
    layout->setContentsMargins(0,0,0,0);
    layout->setSpacing(1);
    layout->addWidget(m_uiPathEdit);
    layout->addWidget(m_uiButton);

    connect(m_uiButton, SIGNAL(clicked(bool)), this, SLOT(slotShowFileDialog()));
    connect(m_fileDialog, &QFileDialog::fileSelected, this, &PathEditBrowser::slotFileSelected);
    connect(m_uiPathEdit, &QLineEdit::textChanged, this, &PathEditBrowser::textChanged);
}

QString PathEditBrowser::text() const
{
    return m_uiPathEdit->text();
}

bool PathEditBrowser::setText(QString string)
{
    m_uiPathEdit->setText(string);
    return true;
}

void PathEditBrowser::clear()
{
    m_uiPathEdit->clear();
}

void PathEditBrowser::slotShowFileDialog()
{
    if(m_fileDialog->isVisible()) {
        m_fileDialog->raise();
        m_fileDialog->activateWindow();
        return;
    }
    QString dir = QStandardPaths::writableLocation(QStandardPaths::HomeLocation);
    if (text().isEmpty()) {
        m_fileDialog->setDirectory(dir);
    } else {
        QFileInfo file(text());
        if (file.isDir()) {
            m_fileDialog->setDirectory(file.canonicalFilePath());
        } else if (file.isFile()) {
            m_fileDialog->setDirectory(file.dir());
            m_fileDialog->selectFile(file.fileName());
        } else {
            m_fileDialog->setDirectory(text());
        }
    }
    m_fileDialog->open();
}

void PathEditBrowser::slotFileSelected(const QString &string)
{
    setText(string);
}

void PathEditBrowser::slotTextChanged(const QString &string)
{
    emit textChanged(string);
}

// uiFileDialog->setNameFilters();
// uiFileDialog->setMimeTypeFilters(mimeFilters);

//case SelectFile:
//    uiFileDialog->setAcceptMode(QFileDialog::AcceptOpen);
//    uiFileDialog->setFileMode(QFileDialog::ExistingFile);
//    break;
//case SelectFolder:
//    uiFileDialog->setAcceptMode(QFileDialog::AcceptOpen);
//    uiFileDialog->setFileMode(QFileDialog::Directory);
//    break;
//case SaveFile:
//    uiFileDialog->setAcceptMode(QFileDialog::AcceptSave);
//    uiFileDialog->setFileMode(QFileDialog::AnyFile);


