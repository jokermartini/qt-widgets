#include "lineeditpath.h"
#include <QLineEdit>
#include <QDragEnterEvent>
#include <QMimeData>
#include <QPushButton>
#include <QStandardPaths>
#include <QFileInfo>
#include <QHBoxLayout>
#include <QApplication>
#include <QClipboard>
#include <QProcess>
#include <QSettings>


/*
- if folder selection mode, it only displays folders in recent menu
- show only filetypes that match current button's filetypes

regex: (?<=\()[^)]*
example: "Image files (*.png *.xpm *.jpg)","Text files (*.txt)","Any files (*)"
*/

// Constructors
LineEditPath::LineEditPath(QWidget *parent) :
    QWidget(parent),
    uiLineEdit(new QLineEdit("", this)),
    uiButton(new QPushButton("...", this)),
    uiFileDialog(new QFileDialog(this)),
    m_directory(QStandardPaths::writableLocation(QStandardPaths::HomeLocation)),
    m_pathMode(SelectFile)
{
    init();
}

LineEditPath::LineEditPath(LineEditPath::PathMode pathMode, QWidget *parent) :
    QWidget(parent),
    uiLineEdit(new QLineEdit("", this)),
    uiButton(new QPushButton(this)),
    uiFileDialog(new QFileDialog(this)),
    m_directory(QStandardPaths::writableLocation(QStandardPaths::HomeLocation)),
    m_pathMode(SelectFile)
{
    setPathMode(pathMode);
    init();
}

void LineEditPath::init() {
    // setup widget
    setAcceptDrops(true);

    // setup line edit
    uiLineEdit->setClearButtonEnabled(true);

    // setup browser button
    uiButton->setText("...");
    uiButton->setFixedWidth(30);
    uiButton->setFixedHeight(24);
    uiButton->setCursor(Qt::CursorShape::PointingHandCursor);
    uiButton->setFocusPolicy(Qt::FocusPolicy::NoFocus);
    uiButton->setToolTip("Right-Click to copy path to clipboard");
    uiButton->setContextMenuPolicy(Qt::CustomContextMenu);

    // setup layout
    QHBoxLayout *layout = new QHBoxLayout(this);
    layout->setContentsMargins(QMargins());
    layout->setSpacing(0);
    layout->addWidget(uiLineEdit);
    layout->addWidget(uiButton);
    setLayout(layout);

    // Menus/Actions
    createActions();
    createMenus();

    // connections
    connect(uiButton, &QPushButton::clicked, this, &LineEditPath::showDialog);
    connect(uiFileDialog, &QFileDialog::fileSelected, this, &LineEditPath::dialogFileSelected);
    connect(uiLineEdit, &QLineEdit::textChanged, this, &LineEditPath::inputChanged);
    connect(uiButton, &QPushButton::customContextMenuRequested, this, &LineEditPath::showContextMenu);
}

QString LineEditPath::path() const
{
    return uiLineEdit->text();
}

bool LineEditPath::setPath(QString path)
{
    if (uiLineEdit->text() == path)
        return true;

    uiLineEdit->setText(path);
    emit pathChanged(QString());
    return true;
}

void LineEditPath::clear()
{
    uiLineEdit->clear();
    emit pathChanged(QString());
}


// Events
void LineEditPath::dragEnterEvent(QDragEnterEvent *event)
{
    const QMimeData *mimeData = event->mimeData();
    if (mimeData->hasUrls() && mimeData->urls().count()==1) {
        event->accept();
    } else {
        event->ignore();
    }
}

void LineEditPath::dropEvent(QDropEvent *event)
{
    const QMimeData *mimeData = event->mimeData();
    if (mimeData->hasUrls()) {
        uiLineEdit->setText(mimeData->urls().first().toLocalFile());
    }
    event->accept();
}

void LineEditPath::resizeEvent(QResizeEvent *event)
{
    //int height = uiInput->sizeHint().height();
    //uiBrowseButton->setFixedHeight(height);
    QWidget::resizeEvent(event);
}

// Methods
LineEditPath::PathMode LineEditPath::pathMode() const
{
    return m_pathMode;
}

void LineEditPath::setPathMode(const PathMode &pathMode)
{
    if (m_pathMode == pathMode)
            return;
    uiLineEdit->clear();
    m_pathMode = pathMode;
}

void LineEditPath::dialogFileSelected(const QString &file)
{
    uiLineEdit->setText(file);
    appendRecentFile(file);
}

void LineEditPath::showDialog()
{
    if(uiFileDialog->isVisible()) {
        uiFileDialog->raise();
        uiFileDialog->activateWindow();
        return;
    }

    switch(pathMode()) {
    case SelectFile:
        uiFileDialog->setAcceptMode(QFileDialog::AcceptOpen);
        uiFileDialog->setFileMode(QFileDialog::ExistingFile);
        break;
    case SelectFolder:
        uiFileDialog->setAcceptMode(QFileDialog::AcceptOpen);
        uiFileDialog->setFileMode(QFileDialog::Directory);
        break;
    case SaveFile:
        uiFileDialog->setAcceptMode(QFileDialog::AcceptSave);
        uiFileDialog->setFileMode(QFileDialog::AnyFile);
        break;
    default:
        Q_UNREACHABLE();
    }

    QString curPath = uiLineEdit->text();
    if(curPath.isEmpty()) {
        uiFileDialog->setDirectory(m_directory);
    } else {
        QFileInfo info(curPath);
        if (info.isDir()){
            uiFileDialog->setDirectory(curPath);
        } else if (info.isFile()) {
            uiFileDialog->setDirectory(info.dir());
            uiFileDialog->selectFile(info.fileName());
        }
    }
    uiFileDialog->open();
}

QStringList LineEditPath::nameFilters() const
{
    return uiFileDialog->nameFilters();
}

void LineEditPath::setNameFilters(QStringList nameFilters)
{
    uiFileDialog->setNameFilters(nameFilters);
}

QStringList LineEditPath::mimeTypeFilters() const
{
    return uiFileDialog->mimeTypeFilters();
}

void LineEditPath::setMimeTypeFilters(QStringList mimeFilters)
{
    uiFileDialog->setMimeTypeFilters(mimeFilters);
}

void LineEditPath::inputChanged(const QString &path)
{
    emit pathChanged(path);
}

void LineEditPath::createActions()
{
    showInExplorerAct = new QAction(tr("&Show In Explorer"), this);
    showInExplorerAct->setShortcut(tr("Ctrl+Shift+S"));
    showInExplorerAct->setStatusTip(tr("Shows path in explorer"));
    connect(showInExplorerAct, &QAction::triggered, this, &LineEditPath::showInExplorer);

    copyToClipboardAct = new QAction(tr("&Copy To Clipboard"), this);
    copyToClipboardAct->setStatusTip(tr("Copies path to clipboard"));
    connect(copyToClipboardAct, &QAction::triggered, this, &LineEditPath::copyToClipboard);

    // recent menu actions
    for (int i = 0; i < MaxRecentFiles; ++i) {
        recentFileActs[i] = new QAction(this);
        recentFileActs[i]->setVisible(false);
        connect(recentFileActs[i], &QAction::triggered, this, &LineEditPath::clickedRecentFile);
    }
}

void LineEditPath::createMenus()
{
    fileMenu = new QMenu(tr("&File"), this);
    fileMenu->addAction(showInExplorerAct);
    fileMenu->addAction(copyToClipboardAct);

    // recent menu
    recentFilesMenu = fileMenu->addMenu(tr("&Recent Files"));
    for (int i = 0; i < MaxRecentFiles; ++i)
    {
        recentFilesMenu->addAction(recentFileActs[i]);
    }
    createRecentFileMenu();
}

void LineEditPath::showContextMenu()
{
    createRecentFileMenu();
    fileMenu->exec(QCursor::pos());
}

void LineEditPath::copyToClipboard()
{
    QClipboard *clip = QApplication::clipboard();
    QString input = uiLineEdit->text();
    clip->setText(input);
}

void LineEditPath::showInExplorer()
{
    QString path = uiLineEdit->text();
    QStringList args;
    args << "/select," << QDir::toNativeSeparators(path);
    QProcess *process = new QProcess(this);
    process->start("explorer.exe", args);
}

void LineEditPath::clickedRecentFile()
{
    QAction *action = qobject_cast<QAction *>(sender());
    if (action)
        uiLineEdit->setText(action->data().toString());
}

void LineEditPath::appendRecentFile(const QString &path)
{
    QSettings settings("JokerMartini", "Application");
    QStringList files = settings.value("recentFileList").toStringList();
    files.removeAll(path);
    files.prepend(path);
    while (files.size() > MaxRecentFiles)
        files.removeLast();
    settings.setValue("recentFileList", files);
}

void LineEditPath::createRecentFileMenu()
{
    QSettings settings("JokerMartini", "Application");
    QStringList files = settings.value("recentFileList").toStringList();

    int numRecentFiles = qMin(files.size(), (int)MaxRecentFiles);

    for (int i = 0; i < numRecentFiles; ++i) {
        recentFileActs[i]->setText(files[i]);
        recentFileActs[i]->setData(files[i]);
        recentFileActs[i]->setVisible(true);
    }
//    for (int j = numRecentFiles; j < MaxRecentFiles; ++j)
//        recentFileActs[j]->setVisible(false);
}


