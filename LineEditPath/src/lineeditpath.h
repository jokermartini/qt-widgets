/*
Features:
    + Supports single folder or file selection
    + Constructor methods
    + Use for Open/Saving filepaths/folders
    + Set nameFilters
    + Set mimeTypeFilters
    + Context Menu
        + Show in Explorer
        + Copy to Clipboard
Todo:
*/
#ifndef LINEEDITPATH_H
#define LINEEDITPATH_H

#include <QWidget>
#include <QLineEdit>
#include <QFileDialog>
#include <QPushButton>
#include <QString>
#include <QMenu>
#include <QAction>

//! LineEditPath provides a simple way to get a path from a user
class LineEditPath : public QWidget
{
    Q_OBJECT
    Q_PROPERTY(PathMode pathMode READ pathMode WRITE setPathMode)
    Q_PROPERTY(QStringList nameFilters READ nameFilters WRITE setNameFilters)
    Q_PROPERTY(QStringList mimeTypeFilters READ mimeTypeFilters WRITE setMimeTypeFilters)
    Q_PROPERTY(QString path READ path WRITE setPath RESET clear NOTIFY pathChanged)

public:

    //! Set if it's Open file/folder dialog or Save file dialog
    enum PathMode {
        SelectFile,//!< A single, existings file. This is basically "Open file"
        SelectFolder,//!< A single, existing directory. This is basically "Open Folder"
        SaveFile //!< Save to a file "Save File"
    };

    //! Constructors
    explicit LineEditPath(QWidget *parent = nullptr);
    explicit LineEditPath(PathMode pathMode, QWidget *parent = nullptr);

    //! Controls
    QLineEdit *uiLineEdit;
    QPushButton *uiButton;
    QFileDialog *uiFileDialog;

    //! Read/Write Accessor
    QString path() const;
    bool setPath(QString path);
    void clear();

    PathMode pathMode() const;
    void setPathMode(const PathMode &pathMode);

    QStringList nameFilters() const;
    void setNameFilters(QStringList nameFilters);

    QStringList mimeTypeFilters() const;
    void setMimeTypeFilters(QStringList mimeTypeFilters);

    //! General widget setup
    void init();
    //! Shows the QFileDialog so the user can select a path
    void showDialog();

signals:
    //! NOTIFY-ACCESSOR for QPathEdit::path
    void pathChanged(QString path);

public slots:
    void inputChanged(const QString & path = QString());
    void dialogFileSelected(const QString & file);
    void showContextMenu();
    void copyToClipboard();
    void showInExplorer();
    void appendRecentFile(const QString &file);

private slots:
    void clickedRecentFile();

protected:
    void dragEnterEvent(QDragEnterEvent *event) override;
    void dropEvent(QDropEvent *event) override;
    void resizeEvent(QResizeEvent *event) override;

private:
    void createActions();
    void createMenus();
    void createRecentFileMenu();

    QString m_directory;
    PathMode m_pathMode;

    QMenu *fileMenu;
    QMenu *recentFilesMenu;
    QAction *showInExplorerAct;
    QAction *copyToClipboardAct;

    enum { MaxRecentFiles = 10 };
    QAction *recentFileActs[MaxRecentFiles];
};

#endif // LINEEDITPATH_H
