#include "mainwindow.h"
#include "lineeditpath.h"
#include <QVBoxLayout>
#include <QComboBox>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
{
    auto *widget = new QWidget(this);
    auto *lay = new QVBoxLayout(widget);
    auto *ctrlA = new LineEditPath(this);
    auto *ctrlB = new LineEditPath(this);
    auto *list = new QComboBox(this);
    list->addItems(QStringList({"SelectFile", "SelectFolder", "SaveFile"}));

    // file types
    QStringList filters;
    filters << "Image files (*.png *.xpm *.jpg)"
            << "Text files (*.txt)"
            << "Any files (*)";
    ctrlA->setNameFilters(filters);

    ctrlB->setNameFilters(QStringList() << "*.vx" << "*.xml");

    lay->addWidget(list);
    lay->addWidget(ctrlA);
    lay->addWidget(ctrlB);
    lay->addStretch();
    setCentralWidget(widget);

    connect(list, static_cast<void (QComboBox::*)(int)>(&QComboBox::currentIndexChanged), [ctrlA](int index){
            ctrlA->setPathMode(static_cast<LineEditPath::PathMode>(index));
        });
}

MainWindow::~MainWindow()
{

}
