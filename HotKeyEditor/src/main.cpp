#include "HotkeyEditorWindow.h"
#include <QApplication>
#include <QStandardPaths>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    HotkeyEditorWindow w({{"save", "Ctrl+S"},
            {"open", "Ctrl+O"},
            {"close", "Ctrl+Q"},
            {"align.top", ""},
            {"aling.bottom", ""},
            {"aling.left", ""},
            {"align.right", ""},
            {"align.center", "Ctrl+O"}},
             QStandardPaths::writableLocation(QStandardPaths::DocumentsLocation)
             + "/Vexus/presets/hotkeys.vhk");
    w.show();

    return a.exec();
}
