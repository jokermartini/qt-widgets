#include "delegate.h"
#include "keysequenceedit.h"

Delegate::Delegate(QObject *parent):
    QStyledItemDelegate(parent)
{

}

QWidget *Delegate::createEditor(QWidget *parent,
                                const QStyleOptionViewItem &option,
                                const QModelIndex &index) const
{
    auto editor = new KeySequenceEdit(parent);
    QFont font  = qvariant_cast<QFont>(index.data(Qt::FontRole));
    editor->setFont(font);
    return editor;
}

void Delegate::setEditorData(   QWidget *editor,
                                const QModelIndex &index) const
{
    QString text = index.data(Qt::EditRole).toString();
    KeySequenceEdit * ks_editor = qobject_cast<KeySequenceEdit *>(editor);
    ks_editor->setText(text);
}

void Delegate::setModelData(QWidget *editor,
                            QAbstractItemModel *model,
                            const QModelIndex &index) const
{
    KeySequenceEdit * ks_editor = qobject_cast<KeySequenceEdit *>(editor);
    model->setData(index, ks_editor->text());
}
