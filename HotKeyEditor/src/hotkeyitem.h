#ifndef HOTKEYITEM_H
#define HOTKEYITEM_H

#include <QString>
#include <QMap>

class HotkeyItem
{
public:
    HotkeyItem(QString command,
               QString shortcut);
    void reset();
    QMap<QString, QString> asdict();
    QString command;
    QString shortcut;
private:
    QString _default;
};

#endif // HOTKEYITEM_H
