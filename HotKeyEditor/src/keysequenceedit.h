#ifndef KEYSEQUENCEEDIT_H
#define KEYSEQUENCEEDIT_H

#include <QLineEdit>

class KeySequenceEdit : public QLineEdit
{
    Q_OBJECT
public:
    KeySequenceEdit(QWidget *parent = nullptr);
protected:
    void keyPressEvent(QKeyEvent *event);
};

#endif // KEYSEQUENCEEDIT_H
