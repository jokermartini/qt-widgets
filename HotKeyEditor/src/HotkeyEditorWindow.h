#ifndef Dialog_H
#define Dialog_H

#include <QDialog>
#include <QMap>

class QMenuBar;
class QDialogButtonBox;
class QTableView;
class HotkeysModel;
class HotKeysView;

class HotkeyEditorWindow  : public QDialog
{   
    Q_OBJECT
public:
    explicit HotkeyEditorWindow (QMap<QString, QString> hotKeys,
                    QString filePath,
                    QWidget *parent = Q_NULLPTR, Qt::WindowFlags f = Qt::WindowFlags());
    ~HotkeyEditorWindow();
private:

    QMap<QString, QString> hotKeys;
    QString filePath;
    void setHotkeys(QMap<QString, QString> hotKeys);

    //gui elements that used
    //not only in create GUI
    HotKeysView * hotKeysTable;
    HotkeysModel * hotKeysModel;

    //init gui
    void createGUI();
    QMenuBar *createMenus();
    QTableView *createHotKeysTable();
    QDialogButtonBox *createDialogButtonBox();


private slots:
    void resetHotkeys();
    void loadHotkeysPreset(QString filepath="");
    void saveHotkeysPreset(QString filepath="");
    void accepted();
};

#endif // Dialog_H
