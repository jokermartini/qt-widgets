#include "hotkeysmodel.h"
#include <QModelIndex>
#include <QColor>
#include <QBrush>
#include <QFont>

HotkeysModel::HotkeysModel(QObject *parent):
    QAbstractTableModel(parent)
{

}

QVariant HotkeysModel::headerData(  int section,
                                    Qt::Orientation orientation,
                                    int role) const
{
    if (orientation == Qt::Horizontal)
    {
        if (role == Qt::DisplayRole)
        {
            if (section < headers.size())
                return headers[section];
        }
    }
    return QVariant();
}

int HotkeysModel::rowCount(const QModelIndex &parent) const
{
    if (parent.isValid())
        return 0;
    return items.size();
}

int HotkeysModel::columnCount(const QModelIndex &parent) const
{
    if (parent.isValid())
        return 0;
    return headers.size();
}

void HotkeysModel::addItem(const HotkeyItem & item)
{
    beginInsertRows(QModelIndex(), rowCount(),rowCount());
    items.append(item);
    endInsertRows();
}

QVariant HotkeysModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    int row = index.row();
    int col = index.column();

    if (0 <= row && row < rowCount())
    {
        auto item = items[row];

        if (role == Qt::DisplayRole
                ||
            role == Qt::EditRole)
        {
            if (col == 0)
            {
                return item.command;
            }
            else if (col == 1)
            {
                return item.shortcut;
            }
        }
        if (role == Qt::BackgroundRole)
        {
            if (hasDuplicate(item))
                return QBrush(QColor(255, 50, 50, 255));
            else
                return QBrush();
        }
        if (role == Qt::FontRole)
        {
            if (hasDuplicate(item))
            {
                QFont fnt = QFont();
                fnt.setBold(true);
                fnt.setItalic(true);
                return fnt;
            }
            else
                return QFont();
        }
    }
    return QVariant();
}

bool HotkeysModel::setData(const QModelIndex &index,
                           const QVariant &value,
                           int role)
{
    if (!index.isValid())
        return QAbstractTableModel::setData(index, value, role);

    int row = index.row();
    int col = index.column();

    if (role == Qt::DisplayRole
            ||
        role == Qt::EditRole)
    {
        if (0 <= row && row < rowCount()
                &&
            0 <= col && col < columnCount())
        {
            if (col == 0)
                items[row].command = value.toString();
            else if (col == 1)
                items[row].shortcut = value.toString();
            emit dataChanged(index, index, {Qt::DisplayRole, Qt::EditRole});
            return true;
        }
    }


    return QAbstractTableModel::setData(index, value, role);
}

//reset all rows
void HotkeysModel::reset()
{
    for(int iR=0; iR<items.size(); iR++)
    {
        items[iR].reset();
    }
}

void HotkeysModel::reset(int row)
{
    items[row].reset();
}

Qt::ItemFlags HotkeysModel::flags(const QModelIndex &index) const
{
    Qt::ItemFlags fl = Qt::NoItemFlags;
    if (index.isValid())
    {
        fl |= Qt::ItemIsEnabled | Qt::ItemIsSelectable;
        if (index.column() == 1)
            fl |= Qt::ItemIsEditable;
    }
    return fl;
}

HotkeyItem HotkeysModel::getItem(int row)
{
    if (0 <= row && row < rowCount())
        return items[row];
    else
        return HotkeyItem("","");
}

bool HotkeysModel::hasDuplicate(const HotkeyItem &item) const
{
    bool bHasDup = false;
    if(!item.shortcut.isEmpty())
    {
        QStringList shortcuts;
        foreach(auto it, items)
        {
            shortcuts << it.shortcut;
        }
        int dups = shortcuts.count(item.shortcut);
        if (dups > 1)
            bHasDup = true;
    }
    return bHasDup;
}

void HotkeysModel::clear()
{
    beginResetModel();
    items = {};
    endResetModel();
}
