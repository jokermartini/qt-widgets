#include "hotkeyitem.h"

HotkeyItem::HotkeyItem(QString command,
                       QString shortcut):
    command(command),
    shortcut(shortcut),
    _default(shortcut)
{

}

QMap<QString, QString> HotkeyItem::asdict()
{
    return {{"command",command},
            {"shortcut",shortcut}};
}

void HotkeyItem::reset()
{
    shortcut = _default;
}
