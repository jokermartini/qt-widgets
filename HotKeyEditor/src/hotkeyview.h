#ifndef HOTKEYLISTVIEW_H
#define HOTKEYLISTVIEW_H

#include <QTableView>

class HotKeysView : public QTableView
{
    Q_OBJECT
public:
    explicit HotKeysView(QWidget *parent = nullptr);
    void closeCurrentEditor();
private slots:
    void openEditor(const QModelIndex &index);
};

#endif // HOTKEYLISTVIEW_H
