#include "keysequenceedit.h"
#include <QKeyEvent>
#include <QDebug>

KeySequenceEdit::KeySequenceEdit(QWidget *parent):
    QLineEdit(parent)
{

}

void KeySequenceEdit::keyPressEvent(QKeyEvent *event)
{
    int keyInt = event->key();
    Qt::Key key = static_cast<Qt::Key>(keyInt);

    if (key == Qt::Key_unknown)
        return;

    if (key == Qt::Key_Escape)
    {
        QLineEdit::keyPressEvent(event);
        return;
    }

    if (key == Qt::Key_Enter || key == Qt::Key_Return)
    {
        QLineEdit::keyPressEvent(event);
        return;
    }

    if (key == Qt::Key_Delete ||
        key == Qt::Key_Backspace)
    {
        setText("");
        return;
    }

    // the user have clicked just and only the special keys
    // Ctrl, Shift, Alt, Meta.
    if (key == Qt::Key_Control ||
        key == Qt::Key_Shift ||
        key == Qt::Key_Alt ||
        key == Qt::Key_Meta)
        return;

    auto modifiers = event->modifiers();
    if (modifiers & Qt::ShiftModifier)
            keyInt += Qt::SHIFT;
    if (modifiers & Qt::ControlModifier)
        keyInt += Qt::CTRL;
    if (modifiers & Qt::AltModifier)
        keyInt += Qt::ALT;
    if (modifiers & Qt::MetaModifier)
        keyInt += Qt::META;

    // set the text edit
    auto keySequence = QKeySequence(keyInt);
    qDebug() << "New KeySequence:" << keySequence.toString(QKeySequence::PortableText);
    setText(keySequence.toString(QKeySequence::PortableText));
}
