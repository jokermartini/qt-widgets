#ifndef HOTKEYSMODEL_H
#define HOTKEYSMODEL_H

#include <QAbstractTableModel>
#include <QList>
#include <QStringList>
#include "hotkeyitem.h"

class HotkeysModel : public QAbstractTableModel
{
    Q_OBJECT
public:
    HotkeysModel(QObject *parent = nullptr);
    void clear();
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    int columnCount(const QModelIndex &parent = QModelIndex()) const override;
    QVariant headerData(int section,
                        Qt::Orientation orientation,
                        int role = Qt::DisplayRole) const override;
    void addItem(const HotkeyItem & item);
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
    bool setData(const QModelIndex &index, const QVariant &value, int role = Qt::EditRole) override;
    Qt::ItemFlags flags(const QModelIndex &index) const override;
    HotkeyItem getItem(int row);
    void reset(int row);
    //reset all rows
    void reset();
private:
    QList<HotkeyItem> items;
    bool hasDuplicate(const HotkeyItem & item) const;
    QStringList headers {"Command","Hotkey"};
};

#endif // HOTKEYSMODEL_H
