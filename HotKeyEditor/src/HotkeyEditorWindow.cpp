#include "HotkeyEditorWindow.h"

#include <QDebug>

#include <QTableView>
#include <QPushButton>
#include <QDialogButtonBox>
#include <QVBoxLayout>
#include <QMenuBar>
#include <QAction>
#include <QMenu>
#include <QTableView>
#include <QHeaderView>
#include <QSortFilterProxyModel>
#include <QFileDialog>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QFile>
#include <QMessageBox>


#include "hotkeyview.h"
#include "hotkeysmodel.h"
#include "delegate.h"

HotkeyEditorWindow::HotkeyEditorWindow(QMap<QString, QString> hotKeys,
               QString filePath,
               QWidget *parent, Qt::WindowFlags f) :
    QDialog(parent,f),
    hotKeys(hotKeys),
    filePath(filePath)
{
    createGUI();

    // initialize the hotkeys and then load the users initial overrides
    // this order is very important otherwise the users overrides would be ignored
    setHotkeys(hotKeys);
    loadHotkeysPreset(filePath);
}

void HotkeyEditorWindow::setHotkeys(QMap<QString, QString> hotKeys)
{
    hotKeysModel->clear();

    foreach(QString action, hotKeys.keys())
        hotKeysModel->addItem(HotkeyItem( action,hotKeys[action]));

    hotKeysTable->sortByColumn(0, Qt::AscendingOrder);
    hotKeysTable->resizeColumnsToContents();
}

void HotkeyEditorWindow::createGUI()
{
    resize(400, 400);
    setWindowTitle("Hotkeys");

    QVBoxLayout * layout = new QVBoxLayout(this);
    layout->addWidget(createHotKeysTable());
    layout->addWidget(createDialogButtonBox());
    layout->setMenuBar(createMenus());
    this->setLayout(layout);

    setStyleSheet(  "QTableView {"
                        "selection-background-color: rgb(100,150,250);"
                        "selection-color: white;"
                    "}");
}

void HotkeyEditorWindow::accepted()
{
    /*
    Desciption:
        Save the hotkeys to a file rather than the qsettings
    */
    hotKeysTable->closeCurrentEditor();
    saveHotkeysPreset(filePath);
    close();
}

QMenuBar * HotkeyEditorWindow::createMenus()
{
    QAction * act_reset = new QAction("Reset", this);
    connect(act_reset, &QAction::triggered,
            this, &HotkeyEditorWindow::resetHotkeys);

    QAction * act_import = new QAction("Import...", this);
    connect(act_import, &QAction::triggered,
            this, [&](){loadHotkeysPreset();});

    QAction * act_export = new QAction("Export...", this);
    connect(act_export, &QAction::triggered,
            this, [&](){saveHotkeysPreset();});

    QMenu * file_menu = new QMenu("File", this);

    file_menu->addAction(act_export);
    file_menu->addAction(act_import);
    file_menu->addSeparator();
    file_menu->addAction(act_reset);

    QMenuBar * menuBar = new QMenuBar(this);
    menuBar->addMenu(file_menu);

    menuBar->setMinimumHeight(30);
    menuBar->setMaximumHeight(30);
    return menuBar;
}

QDialogButtonBox * HotkeyEditorWindow::createDialogButtonBox()
{
    QDialogButtonBox * uiButtons = new QDialogButtonBox(this);
    QPushButton * uiOk = new QPushButton("OK", uiButtons);
    QPushButton * uiCancel = new QPushButton("Cancel", uiButtons);
    uiButtons->addButton(uiOk, QDialogButtonBox::ActionRole);
    uiButtons->addButton(uiCancel, QDialogButtonBox::ActionRole);

    connect(uiOk, &QPushButton::clicked,
            this, &HotkeyEditorWindow::accepted);
    connect(uiCancel, &QPushButton::clicked,
            this, &HotkeyEditorWindow::close);

    return uiButtons;
}

QTableView * HotkeyEditorWindow::createHotKeysTable()
{
    //model
    hotKeysModel = new HotkeysModel(this);
    auto proxyModel = new QSortFilterProxyModel(this);
    proxyModel->setFilterKeyColumn(0);
    proxyModel->setSortCaseSensitivity(Qt::CaseInsensitive);
    proxyModel->setSourceModel(hotKeysModel);

    hotKeysTable = new HotKeysView(this);
    hotKeysTable->setSortingEnabled(true);
    hotKeysTable->setAlternatingRowColors(true);
    hotKeysTable->setModel(proxyModel);
    auto delegate = new Delegate(this);
    hotKeysTable->setItemDelegateForColumn(1, delegate);
    hotKeysTable->setSelectionBehavior(QAbstractItemView::SelectRows);
    hotKeysTable->setSelectionMode(QAbstractItemView::SingleSelection);
    hotKeysTable->setEditTriggers(QAbstractItemView::NoEditTriggers);

    hotKeysTable->verticalHeader()->hide();
    hotKeysTable->horizontalHeader()->show();
    hotKeysTable->horizontalHeader()->setStretchLastSection(true);

    return hotKeysTable;
}

void HotkeyEditorWindow::resetHotkeys()
{
    auto result = QMessageBox::question(this, "Hotkeys",
        "This operation will reset to the default hotkeys, "
        "the current user hotkeys will be lost."
        "\nContinue?",
        QMessageBox::Ok,
        QMessageBox::Abort);

    if (QMessageBox::Ok != result)
        return;

    // reset to their default values
    hotKeysModel->reset();
}

void HotkeyEditorWindow::loadHotkeysPreset(QString filepath)
{
    /*
    Description:
        Will load the specified preset filepath overriding any valid hotkeys
    */
    if (filepath.isEmpty())
        filepath = QFileDialog::getOpenFileName(this, "Open", "", "Hotkey Files (*.vxh)");

    QFile file(filepath);
    if(file.open(QIODevice::ReadOnly))
    {
        auto data = file.readAll();
        QJsonDocument doc = QJsonDocument::fromJson(data);
        auto array = doc.array();
        for(int i=0; i<array.size(); i++)
        {
            auto object = array[i].toObject();
            QString command = object.value("command").toString();
            QString shortcut = object.value("shortcut").toString();
            HotkeyItem item(command,
                            shortcut);
            for(int iR=0;
                iR<hotKeysModel->rowCount();
                iR++)
            {
                if(hotKeysModel->data
                  (hotKeysModel->index(iR,0)).toString()
                   ==command)
                {
                    hotKeysModel->setData(
                                hotKeysModel->index(iR,1),
                                shortcut);
                }
            }
        }
        file.close();
    }
}

void HotkeyEditorWindow::saveHotkeysPreset(QString filepath)
{
    if (filepath.isEmpty())
        filepath = QFileDialog::getSaveFileName(this, "Save", "", "Hotkey Files (*.vxh)");

    if (filepath.isEmpty())
        return;

    QFile file(filepath);
    if(file.open(QIODevice::WriteOnly))
    {
        QJsonArray array;
        for(int iR=0; iR<hotKeysModel->rowCount(); iR++)
        {
            QJsonObject obj;
            auto item = hotKeysModel->getItem(iR);
            obj["command"]     = item.command;
            obj["shortcut"]    = item.shortcut;
            array.push_back(QJsonValue(obj));
        }
        QJsonDocument doc(array);
        auto data = doc.toJson(QJsonDocument::Indented);
        file.write(data);
        file.close();
    }

}

HotkeyEditorWindow::~HotkeyEditorWindow()
{

}
