#include "hotkeyview.h"

HotKeysView::HotKeysView(QWidget *parent) :
    QTableView(parent)
{
    connect(this, &HotKeysView::clicked,
            this, &HotKeysView::openEditor);
}

void HotKeysView::closeCurrentEditor()
{
    auto index = currentIndex();
    if (index.isValid())
    {
        auto ix = index.sibling(index.row(), 1);
        auto widget = indexWidget(ix);
        if (widget)
        {
            commitData(widget);
            closeEditor(widget, QAbstractItemDelegate::NoHint);
        }
    }
}

void HotKeysView::openEditor(const QModelIndex &index)
{
    if (index.isValid())
    {
        auto ix = index.sibling(index.row(), 1);
        setCurrentIndex(ix);
        edit(ix);
    }
}
