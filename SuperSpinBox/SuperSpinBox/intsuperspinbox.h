#ifndef INTSUPERSPINBOX_H
#define INTSUPERSPINBOX_H

#include <QWidget>
#include <QSpinBox>

class IntSuperSpinBox : public QSpinBox
{
    Q_OBJECT
public:
    explicit IntSuperSpinBox(QWidget *parent = 0);

    int defaultValue() const;
    void setDefaultValue(const int &value);

    double dragStepMultiplier() const;
    void setDragStepMultiplier(const double &value);

    int mouseStartPosY() const;
    void setMouseStartPosY(const int &value);

    int dragStartValue() const;
    void setDragStartValue(const int &value);

    bool isDragging() const;
    void setIsDragging(const bool &value);

private:
    int m_defaultValue;
    double m_dragStepMultiplier;

    int m_mouseStartPosY;
    int m_dragStartValue;
    bool m_isDragging;

signals:

public slots:

public:
    void stepBy(int steps) override;

protected:
    void mousePressEvent(QMouseEvent *event) override;
    void mouseMoveEvent(QMouseEvent *event) override;
    void mouseReleaseEvent(QMouseEvent *event) override;
    void contextMenuEvent(QContextMenuEvent *event) override;
};

#endif // INTSUPERSPINBOX_H
