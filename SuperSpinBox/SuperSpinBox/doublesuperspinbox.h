#ifndef DOUBLESUPERSPINBOX_H
#define DOUBLESUPERSPINBOX_H

#include <QWidget>
#include <QDoubleSpinBox>

class DoubleSuperSpinBox : public QDoubleSpinBox
{
    Q_OBJECT
public:
    explicit DoubleSuperSpinBox(QWidget *parent = 0);

    double defaultValue() const;
    void setDefaultValue(const double &value);

    double dragStepMultiplier() const;
    void setDragStepMultiplier(const double &value);

    int mouseStartPosY() const;
    void setMouseStartPosY(const int &value);

    double dragStartValue() const;
    void setDragStartValue(const double &value);

    bool isDragging() const;
    void setIsDragging(const bool &value);

private:
    double m_defaultValue;
    double m_dragStepMultiplier;

    int m_mouseStartPosY;
    double m_dragStartValue;
    bool m_isDragging;

signals:

public slots:

protected:
    void mousePressEvent(QMouseEvent *event) override;
    void mouseMoveEvent(QMouseEvent *event) override;
    void mouseReleaseEvent(QMouseEvent *event) override;
    void contextMenuEvent(QContextMenuEvent *event) override;

public:
    void stepBy(int steps) override;
};

#endif // DOUBLESUPERSPINBOX_H
