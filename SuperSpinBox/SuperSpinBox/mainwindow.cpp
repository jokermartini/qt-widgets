#include "mainwindow.h"
#include "intsuperspinbox.h"
#include "doublesuperspinbox.h"
#include <QVBoxLayout>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
{
    // Controls
    auto *widget_main = new QWidget();
    auto *layout = new QVBoxLayout(widget_main);

    auto *intSpinner = new IntSuperSpinBox(this);
    intSpinner->setRange(-10000,10000);

    auto *doubleSpinner = new DoubleSuperSpinBox(this);
    doubleSpinner->setRange(-1000,1000);

    auto *doubleSpinnerB = new DoubleSuperSpinBox(this);
    doubleSpinnerB->setRange(0.0,1.0);
    doubleSpinnerB->setDragStepMultiplier(.005);

    // Layout
    layout->addWidget(intSpinner);
    layout->addWidget(doubleSpinner);
    layout->addWidget(doubleSpinnerB);
    layout->addStretch();
    widget_main->setLayout(layout);
    setCentralWidget(widget_main);
    resize(300,300);
}

MainWindow::~MainWindow()
{

}
