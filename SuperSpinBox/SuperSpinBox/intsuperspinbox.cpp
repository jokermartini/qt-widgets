#include "intsuperspinbox.h"
#include <QMouseEvent>
#include <QStyleOptionSpinBox>
#include <QCommonStyle>
#include <QStyle>
#include <QDebug>

IntSuperSpinBox::IntSuperSpinBox(QWidget *parent) :
    QSpinBox(parent),
    m_defaultValue(0),
    m_dragStepMultiplier(0.5),
    m_isDragging(false)
{
    setMouseTracking(true);
}

int IntSuperSpinBox::defaultValue() const
{
    return m_defaultValue;
}

void IntSuperSpinBox::setDefaultValue(const int &value)
{
    m_defaultValue = value;
}

double IntSuperSpinBox::dragStepMultiplier() const
{
    return m_dragStepMultiplier;
}

void IntSuperSpinBox::setDragStepMultiplier(const double &value)
{
    m_dragStepMultiplier = value;
}

int IntSuperSpinBox::mouseStartPosY() const
{
    return m_mouseStartPosY;
}

void IntSuperSpinBox::setMouseStartPosY(const int &value)
{
    m_mouseStartPosY = value;
}

int IntSuperSpinBox::dragStartValue() const
{
    return m_dragStartValue;
}

void IntSuperSpinBox::setDragStartValue(const int &value)
{
    m_dragStartValue = value;
}

bool IntSuperSpinBox::isDragging() const
{
    return m_isDragging;
}

void IntSuperSpinBox::setIsDragging(const bool &value)
{
    m_isDragging = value;
}

void IntSuperSpinBox::stepBy(int steps)
{
    if (!isDragging()) {
        QSpinBox::stepBy(steps);
    }
}

void IntSuperSpinBox::mousePressEvent(QMouseEvent *event)
{
    setMouseStartPosY(event->pos().y());
    setDragStartValue(this->value());
    setIsDragging(false);
    QSpinBox::mousePressEvent(event);
}

void IntSuperSpinBox::mouseMoveEvent(QMouseEvent *event)
{
    if (event->pos().y() < this->rect().top() || event->pos().y() > this->rect().bottom() || this->isDragging())
    {
        setCursor(Qt::SizeVerCursor);
        setIsDragging(true);

        // Modifier overrides
        double mult = dragStepMultiplier();

        switch (event->modifiers()) {
            case Qt::ControlModifier:
                mult = dragStepMultiplier() * 3.0;
                break;
            case Qt::AltModifier:
                mult = dragStepMultiplier() * 0.2;
                break;
            default:
                mult = dragStepMultiplier();
                break;
        }

        // Set value
        int dragDelta = (mouseStartPosY() - event->pos().y()) * mult;
        setValue(dragStartValue() + dragDelta);
    }
    else
    {
        setDragStartValue(this->value());
    }
    QSpinBox::mouseMoveEvent(event);
}

void IntSuperSpinBox::mouseReleaseEvent(QMouseEvent *event)
{
    QSpinBox::mouseReleaseEvent(event);
    unsetCursor();
    setIsDragging(false);
}

void IntSuperSpinBox::contextMenuEvent(QContextMenuEvent *event)
{
    QStyleOptionSpinBox opt;
    opt.initFrom(this);

    // Combine up and down arrows to determine if cursor is within buttons
    QRect rect;
    rect = rect.united(this->style()->subControlRect(QStyle::CC_SpinBox, &opt, QStyle::SC_SpinBoxUp, this));
    rect = rect.united(this->style()->subControlRect(QStyle::CC_SpinBox, &opt, QStyle::SC_SpinBoxDown, this));

    if (rect.contains(event->pos())) {
        setValue(this->defaultValue());
        selectAll();
    }
    else
    {
        QSpinBox::contextMenuEvent(event);
    }
}
