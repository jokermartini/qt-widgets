#include "doublesuperspinbox.h"
#include <QMouseEvent>
#include <QStyleOptionSpinBox>
#include <QCommonStyle>
#include <QStyle>
#include <QDebug>

DoubleSuperSpinBox::DoubleSuperSpinBox(QWidget *parent) :
    QDoubleSpinBox(parent),
    m_defaultValue(0),
    m_dragStepMultiplier(0.005),
    m_isDragging(false)
{
    setMouseTracking(true);
}

double DoubleSuperSpinBox::defaultValue() const
{
    return m_defaultValue;
}

void DoubleSuperSpinBox::setDefaultValue(const double &value)
{
    m_defaultValue = value;
}

double DoubleSuperSpinBox::dragStepMultiplier() const
{
    return m_dragStepMultiplier;
}

void DoubleSuperSpinBox::setDragStepMultiplier(const double &value)
{
    m_dragStepMultiplier = value;
}

int DoubleSuperSpinBox::mouseStartPosY() const
{
    return m_mouseStartPosY;
}

void DoubleSuperSpinBox::setMouseStartPosY(const int &value)
{
    m_mouseStartPosY = value;
}

double DoubleSuperSpinBox::dragStartValue() const
{
    return m_dragStartValue;
}

void DoubleSuperSpinBox::setDragStartValue(const double &value)
{
    m_dragStartValue = value;
}

bool DoubleSuperSpinBox::isDragging() const
{
    return m_isDragging;
}

void DoubleSuperSpinBox::setIsDragging(const bool &value)
{
    m_isDragging = value;
}

void DoubleSuperSpinBox::stepBy(int steps)
{
    if (!isDragging()) {
        QDoubleSpinBox::stepBy(steps);
    }
}

void DoubleSuperSpinBox::mousePressEvent(QMouseEvent *event)
{
    setMouseStartPosY(event->pos().y());
    setDragStartValue(this->value());
    setIsDragging(false);
    QDoubleSpinBox::mousePressEvent(event);
}

void DoubleSuperSpinBox::mouseMoveEvent(QMouseEvent *event)
{
    if (event->pos().y() < this->rect().top() || event->pos().y() > this->rect().bottom() || this->isDragging())
    {
        setCursor(Qt::SizeVerCursor);
        setIsDragging(true);

        // Modifier overrides
        double mult = dragStepMultiplier();

        switch (event->modifiers()) {
            case Qt::ControlModifier:
                mult = dragStepMultiplier() * 3.0;
                break;
            case Qt::AltModifier:
                mult = dragStepMultiplier() * 0.01;
                break;
            default:
                mult = dragStepMultiplier();
                break;
        }

        // Set value
        double dragDelta = (mouseStartPosY() - event->pos().y()) * mult;
        setValue(dragStartValue() + dragDelta);
    }
    else
    {
        setDragStartValue(this->value());
    }
    QDoubleSpinBox::mouseMoveEvent(event);
}

void DoubleSuperSpinBox::mouseReleaseEvent(QMouseEvent *event)
{
    QDoubleSpinBox::mouseReleaseEvent(event);
    unsetCursor();
    setIsDragging(false);
}

void DoubleSuperSpinBox::contextMenuEvent(QContextMenuEvent *event)
{
    QStyleOptionSpinBox opt;
    opt.initFrom(this);

    // Combine up and down arrows to determine if cursor is within buttons
    QRect rect;
    rect = rect.united(this->style()->subControlRect(QStyle::CC_SpinBox, &opt, QStyle::SC_SpinBoxUp, this));
    rect = rect.united(this->style()->subControlRect(QStyle::CC_SpinBox, &opt, QStyle::SC_SpinBoxDown, this));

    if (rect.contains(event->pos())) {
        setValue(this->defaultValue());
        selectAll();
    }
    else
    {
        QDoubleSpinBox::contextMenuEvent(event);
    }
}
