#include "mainwindow.h"
#include <QApplication>
#include "includeexcludedialog.h"
#include <QFile>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    IncludeExcludeDialog w;
    w.show();

    QFile file(QCoreApplication::applicationDirPath() + "/stylesheet.css");
    if (file.exists()) {
        file.open(QFile::ReadOnly);
        QString StyleSheet = QLatin1String(file.readAll());
        qApp->setStyleSheet(StyleSheet);
    }

    return a.exec();
}
