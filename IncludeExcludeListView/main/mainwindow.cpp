#include "mainwindow.h"
#include "includeexcludedialog.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
{
    IncludeExcludeDialog w();
    w.show();
}

MainWindow::~MainWindow()
{

}
