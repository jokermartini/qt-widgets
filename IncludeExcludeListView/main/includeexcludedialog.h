#ifndef INCLUDEEXCLUDEDIALOG_H
#define INCLUDEEXCLUDEDIALOG_H

#include <QDialog>
#include <QLineEdit>
#include <QListWidget>
#include <QPushButton>
#include <QString>

class IncludeExcludeDialog : public QDialog
{
    Q_OBJECT

    public:
        explicit IncludeExcludeDialog(QWidget *parent = nullptr,
                                      Qt::WindowFlags f = Qt::WindowFlags());
        ~IncludeExcludeDialog();

        QLineEdit *uiSearch;
        QListWidget *uiAvailableList;
        QPushButton *uiAppend;
        QPushButton *uiRemove;
        QPushButton *uiEquals;
        QPushButton *uiReset;
        QLineEdit *uiActiveInput;
        QListWidget *uiActiveList;

        // Adding items to lists
        void setAvailableItems(const QString text);
        void setActiveItems(const QString text);

    private:
        void init();
        void filterAvailableList(const QString text);
        void updateControls();

        bool isPatternMatch(const QString text, QStringList patterns);
        void highlightItems();

    public slots:
        void slotAppendAvailableSelection();
        void slotClearActiveList();
        void slotRemoveActiveSelection();
        void slotSetActiveFromSelection();
        void slotActiveInputEntered();
        void slotActiveInputChanged();

    protected:
        void keyPressEvent(QKeyEvent *event) override;
};

#endif // INCLUDEEXCLUDEDIALOG_H
