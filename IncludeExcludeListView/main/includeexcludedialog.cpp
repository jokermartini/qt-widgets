#include "includeexcludedialog.h"
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QLabel>
#include <QDialogButtonBox>
#include <QRegExp>
#include <QKeyEvent>

/*!
Improve the appending of items from one list to another along with the
method used for highlighting items in the Available list. Currently
the solutions are very slow when append large lists of items.
 */


IncludeExcludeDialog::IncludeExcludeDialog( QWidget *parent, Qt::WindowFlags f) :
    QDialog(parent,f)
{
    init();
}


IncludeExcludeDialog::~IncludeExcludeDialog()
{
}


void IncludeExcludeDialog::init()
{
    resize(500, 400);
    setWindowTitle("Include/Exclude");

    // Controls: Left
    QLabel *uiAvailableLabel = new QLabel("Available", this);

    uiSearch = new QLineEdit(this);
    uiSearch->setClearButtonEnabled(true);
    uiSearch->setPlaceholderText("Search...");

    uiAvailableList = new QListWidget(this);
    uiAvailableList->setSelectionMode(QAbstractItemView::ExtendedSelection);
    uiAvailableList->setSortingEnabled(true);

    QVBoxLayout *leftLayout = new QVBoxLayout();
    leftLayout->addWidget(uiAvailableLabel);
    leftLayout->addWidget(uiSearch);
    leftLayout->addWidget(uiAvailableList);

    // Controls: Center
    uiAppend = new QPushButton(">");
    uiAppend->setToolTip("Appends selection to Active list");
    uiAppend->setFixedSize(30,30);

    uiRemove = new QPushButton("<");
    uiRemove->setToolTip("Removes selection from Active list");
    uiRemove->setFixedSize(30,30);

    uiEquals = new QPushButton("=");
    uiEquals->setToolTip("Sets Active list to match Available selection");
    uiEquals->setFixedSize(30,30);

    QFrame *uiSeparator = new QFrame();
    uiSeparator->setFrameShape(QFrame::HLine);
    uiSeparator->setFrameShadow(QFrame::Sunken);

    uiReset = new QPushButton("X");
    uiReset->setToolTip("Clears the Active list");
    uiReset->setFixedSize(30,30);

    QVBoxLayout *centerLayout = new QVBoxLayout();
    centerLayout->addStretch();
    centerLayout->addWidget(uiAppend);
    centerLayout->addWidget(uiRemove);
    centerLayout->addWidget(uiEquals);
    centerLayout->addWidget(uiSeparator);
    centerLayout->addWidget(uiReset);
    centerLayout->addStretch();

    // Controls: Right
    QLabel *uiActiveLabel = new QLabel("Active", this);

    uiActiveInput = new QLineEdit(this);
    uiActiveInput->setClearButtonEnabled(true);
    uiActiveInput->setPlaceholderText("Add New...");

    uiActiveList = new QListWidget(this);
    uiActiveList->setSelectionMode(QAbstractItemView::ExtendedSelection);
    uiActiveList->setSortingEnabled(true);

    QVBoxLayout *rightLayout = new QVBoxLayout();
    rightLayout->addWidget(uiActiveLabel);
    rightLayout->addWidget(uiActiveInput);
    rightLayout->addWidget(uiActiveList);

    // Content Layout
    QHBoxLayout *contentLayout = new QHBoxLayout();
    contentLayout->addLayout(leftLayout);
    contentLayout->addLayout(centerLayout);
    contentLayout->addLayout(rightLayout);

    // ButtonBox
    QDialogButtonBox *uiButtonBox = new QDialogButtonBox(this);
    uiButtonBox->setOrientation(Qt::Horizontal);
    uiButtonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);
    uiButtonBox->button(QDialogButtonBox::Ok)->setFixedSize(75,22);
    uiButtonBox->button(QDialogButtonBox::Cancel)->setFixedSize(75,22);

    // Main Layout
    QVBoxLayout *mainLayout = new QVBoxLayout();
    mainLayout->addLayout(contentLayout);
    mainLayout->addWidget(uiButtonBox);
    setLayout(mainLayout);

    // signals-slots
    connect(uiButtonBox, SIGNAL(accepted()), this, SLOT(accept()));
    connect(uiButtonBox, SIGNAL(rejected()), this, SLOT(reject()));
    connect(uiSearch, &QLineEdit::textChanged, this, &IncludeExcludeDialog::filterAvailableList);
    connect(uiAppend, &QPushButton::clicked, this, &IncludeExcludeDialog::slotAppendAvailableSelection);
    connect(uiAvailableList, &QListWidget::itemDoubleClicked, this, &IncludeExcludeDialog::slotAppendAvailableSelection);
    connect(uiReset, &QPushButton::clicked, this, &IncludeExcludeDialog::slotClearActiveList);
    connect(uiRemove, &QPushButton::clicked, this, &IncludeExcludeDialog::slotRemoveActiveSelection);
    connect(uiEquals, &QPushButton::clicked, this, &IncludeExcludeDialog::slotSetActiveFromSelection);
    connect(uiActiveList, &QListWidget::itemDoubleClicked, this, &IncludeExcludeDialog::slotRemoveActiveSelection);
    connect(uiActiveInput, &QLineEdit::returnPressed, this, &IncludeExcludeDialog::slotActiveInputEntered);
    connect(uiActiveInput, &QLineEdit::textChanged, this, &IncludeExcludeDialog::slotActiveInputChanged);
    connect(uiActiveList, &QListWidget::itemSelectionChanged, this, &IncludeExcludeDialog::updateControls);
    connect(uiAvailableList, &QListWidget::itemSelectionChanged, this, &IncludeExcludeDialog::updateControls);

    // begin
    updateControls();

    // Unit test
    setAvailableItems(" ,  John,  john,,  mike,  Kevin, Camera 01, super , Nice, Afghanistan, Albania, Algeria, American Samoa, Andorra, Angola, Anguilla, Antarctica, Antigua and Barbuda, Argentina, Armenia, Aruba, Australia, Austria, Azerbaijan, Bahamas, Bahrain, Bangladesh, Barbados, Belarus, Belgium, Belize, Benin, Bermuda, Bhutan, Bolivia, Bosnia and Herzegovina, Botswana, Bouvet Island, Brazil, British Indian Ocean Territory, Brunei Darussalam, Bulgaria, Burkina Faso, Burundi, Cambodia, Cameroon, Canada, Cape Verde, Cayman Islands, Central African Republic, Chad, Chile, China, Christmas Island, Cocos (Keeling) Islands, Colombia, Comoros, Congo, Congo, The Democratic Republic of The, Cook Islands, Costa Rica, Cote D'ivoire, Croatia, Cuba, Cyprus, Czech Republic, Denmark, Djibouti, Dominica, Dominican Republic, Ecuador, Egypt, El Salvador, Equatorial Guinea, Eritrea, Estonia, Ethiopia, Falkland Islands (Malvinas), Faroe Islands, Fiji, Finland, France, French Guiana, French Polynesia, French Southern Territories, Gabon, Gambia, Georgia, Germany, Ghana, Gibraltar, Greece, Greenland, Grenada, Guadeloupe, Guam, Guatemala, Guinea, Guinea-bissau, Guyana, Haiti, Heard Island and Mcdonald Islands, Holy See (Vatican City State), Honduras, Hong Kong, Hungary, Iceland, India, Indonesia, Iran, Islamic Republic of, Iraq, Ireland, Israel, Italy, Jamaica, Japan, Jordan, Kazakhstan, Kenya, Kiribati, Korea, Democratic People's Republic of, Korea, Republic of, Kuwait, Kyrgyzstan, Lao People's Democratic Republic, Latvia, Lebanon, Lesotho, Liberia, Libyan Arab Jamahiriya, Liechtenstein, Lithuania, Luxembourg, Macao, Macedonia, The Former Yugoslav Republic of, Madagascar, Malawi, Malaysia, Maldives, Mali, Malta, Marshall Islands, Martinique, Mauritania, Mauritius, Mayotte, Mexico, Micronesia, Federated States of, Moldova, Republic of, Monaco, Mongolia, Montserrat, Morocco, Mozambique, Myanmar, Namibia, Nauru, Nepal, Netherlands, Netherlands Antilles, New Caledonia, New Zealand, Nicaragua, Niger, Nigeria, Niue, Norfolk Island, Northern Mariana Islands, Norway, Oman, Pakistan, Palau, Palestinian Territory, Occupied, Panama, Papua New Guinea, Paraguay, Peru, Philippines, Pitcairn, Poland, Portugal, Puerto Rico, Qatar, Reunion, Romania, Russian Federation, Rwanda, Saint Helena, Saint Kitts and Nevis, Saint Lucia, Saint Pierre and Miquelon, Saint Vincent and The Grenadines, Samoa, San Marino, Sao Tome and Principe, Saudi Arabia, Senegal, Serbia and Montenegro, Seychelles, Sierra Leone, Singapore, Slovakia, Slovenia, Solomon Islands, Somalia, South Africa, South Georgia and The South Sandwich Islands, Spain, Sri Lanka, Sudan, Suriname, Svalbard and Jan Mayen, Swaziland, Sweden, Switzerland, Syrian Arab Republic, Taiwan, Province of China, Tajikistan, Tanzania, United Republic of, Thailand, Timor-leste, Togo, Tokelau, Tonga, Trinidad and Tobago, Tunisia, Turkey, Turkmenistan, Turks and Caicos Islands, Tuvalu, Uganda, Ukraine, United Arab Emirates, United Kingdom, United States, United States Minor Outlying Islands, Uruguay, Uzbekistan, Vanuatu, Venezuela, Viet Nam, Virgin Islands, British, Virgin Islands, U.S., Wallis and Futuna, Western Sahara, Yemen, Zambia, Zimbabwe");
    setActiveItems("*s, something");
}

/*!
 * \brief Override to avoid certain key events triggering default actions
 * \param event
 */
void IncludeExcludeDialog::keyPressEvent(QKeyEvent *event)
{
    int keyInt = event->key();
    Qt::Key key = static_cast<Qt::Key>(keyInt);

    if (key == Qt::Key_Escape || key == Qt::Key_Enter || key == Qt::Key_Return)
    {
        return;
    }

    QDialog::keyPressEvent(event);
}

/*!
 * \brief Appends the string, comma separated, to the available list.
 * Duplicates can exist, but empty strings are ignored.
 * \param text
 */
void IncludeExcludeDialog::setAvailableItems(const QString text)
{
    uiAvailableList->clear();

    // split string by comma and leading spaces
    QStringList list = text.split(",");
    foreach (QString str, list) {

        // remove any leading spaces on words
        QString s = str.remove(QRegExp("^([ ]+)"));

        // ignore empty entries
        if (s.isEmpty()) {
            continue;
        }

        uiAvailableList->addItem(s);
    }
}

/*!
 * \brief Appends the string, comma separated, to the active list.
 * Duplicates can exist, but empty strings are ignored.
 * \param text
 */
void IncludeExcludeDialog::setActiveItems(const QString text)
{
    uiActiveList->clear();

    // split string by comma and leading spaces
    QStringList list = text.split(",");
    foreach (QString str, list) {

        // remove any leading spaces on words
        QString s = str.remove(QRegExp("^([ ]+)"));

        // ignore empty entries
        if (s.isEmpty()) {
            continue;
        }

        uiActiveList->addItem(s);
    }
    highlightItems();
}

/*!
 * \brief Method used to apply search filter to available items list
 * \param text: Search word used in filtering
 */
void IncludeExcludeDialog::filterAvailableList(const QString text)
{
    if (text.trimmed().isEmpty()){
        for(int i = 0; i < uiAvailableList->count(); ++i)
        {
            uiAvailableList->item(i)->setHidden(false);
        }
    }else{
        QItemSelectionModel *selection = uiAvailableList->selectionModel();
        selection->clearSelection();

        for(int i = 0; i < uiAvailableList->count(); ++i)
        {
            if (uiAvailableList->item(i)->text().contains(text, Qt::CaseInsensitive)) {
                uiAvailableList->item(i)->setHidden(false);
            }else{
                uiAvailableList->item(i)->setHidden(true);
            }
        }
    }
}

/*!
 * \brief Appends each selected Available item to the Active list
 */
void IncludeExcludeDialog::slotAppendAvailableSelection()
{
    // collect existing list items
    QStringList existingList;
    for(int i = 0; i < uiActiveList->count(); ++i)
    {
        existingList.append(uiActiveList->item(i)->text());
    }

    QList<QListWidgetItem*> selectedItems = uiAvailableList->selectedItems();
    foreach (QListWidgetItem * item, selectedItems) {

        if (item->text().isEmpty()) {
            continue;
        }

        // if it doesn't already exist then append
        if (!existingList.contains(item->text(), Qt::CaseInsensitive)) {
            uiActiveList->addItem(item->text());
        }
    }
    highlightItems();
}

/*!
 * \brief Clears the active list items
 */
void IncludeExcludeDialog::slotClearActiveList()
{
    uiActiveList->clear();
    highlightItems();
}

/*!
 * \brief Removes selected Actives items
 */
void IncludeExcludeDialog::slotRemoveActiveSelection()
{
    QList<QListWidgetItem*> selectedItems = uiActiveList->selectedItems();
    foreach (QListWidgetItem * item, selectedItems) {
        uiActiveList->takeItem(uiActiveList->row(item));
    }
    highlightItems();
}

/*!
 * \brief Enables and Disables ui controls based on certain conditions
 */
void IncludeExcludeDialog::updateControls()
{
    if (!uiAvailableList->selectedItems().empty()) {
        uiAppend->setEnabled(true);
        uiEquals->setEnabled(true);
    }else{
        uiAppend->setEnabled(false);
        uiEquals->setEnabled(false);
    }

    if (!uiActiveList->selectedItems().empty()) {
        uiRemove->setEnabled(true);
    }else{
        uiRemove->setEnabled(false);
    }
}

/*!
 * \brief Sets the Active list to match the Available list's selection
 */
void IncludeExcludeDialog::slotSetActiveFromSelection()
{
    uiActiveList->clear();
    slotAppendAvailableSelection();
}

/*!
 * \brief Comma separates the input field and then appends unique items to the list
 */
void IncludeExcludeDialog::slotActiveInputEntered()
{
    // collect existing list items
    QStringList existingList;
    for(int i = 0; i < uiActiveList->count(); ++i)
    {
        existingList.append(uiActiveList->item(i)->text());
    }

    // add items
    QStringList list = uiActiveInput->text().split(",");
    foreach (QString str, list) {

        // remove any leading spaces on words
        QString s = str.remove(QRegExp("^([ ]+)"));

        // ignore empty entries
        if (s.isEmpty()) {
            continue;
        }

        // if it doesn't already exist then append
        if (!existingList.contains(s, Qt::CaseInsensitive)) {
            uiActiveList->addItem(s);
        }
    }

    uiActiveInput->clear();
    highlightItems();
}

/*!
 * \brief Updates list highlighting showing affected items
 */
void IncludeExcludeDialog::slotActiveInputChanged()
{
    highlightItems();
}

/*!
 * \brief Uses regex to determine if supplied text matches any of the supplied patterns
 * \param text
 * \param patterns: list of strings which can contain regex
 * \return True if match else False
 */
bool IncludeExcludeDialog::isPatternMatch(const QString text, QStringList patterns)
{
    foreach (QString pattern, patterns) {

        // skip empty patterns
        if (pattern.isEmpty()) {
            continue;
        }

        // construct regex pattern
        pattern = QString("^%1$").arg(pattern.toLower());
        if (pattern.contains("*")) {
            //pattern.replace("*", "[^\\s]*");
            pattern.replace("*", "[^+]*");
        }

        QRegExp rx(pattern);
        if (rx.indexIn(text.toLower()) != -1) {
            return true;
        }
    }
    return false;
}

/*!
 * \brief Colorizes items in the Available list indicating if they are affected
 * by entries in the Active list.
 */
void IncludeExcludeDialog::highlightItems()
{
    // collect existing items
    QStringList activeList;
    for(int i = 0; i < uiActiveList->count(); ++i)
    {
        activeList.append(uiActiveList->item(i)->text());
    }

    for(int i = 0; i < uiAvailableList->count(); ++i)
    {
        QListWidgetItem *item = uiAvailableList->item(i);
        QFont fnt = item->font();

        // highlight items which exist in the list already
        if (isPatternMatch(item->text(), activeList)) {
            item->setForeground(QColor(100, 205, 100));
            fnt.setBold(true);
            fnt.setStrikeOut(true);
            item->setFont(fnt);
        }else{
            item->setForeground(palette().color(QPalette::WindowText));
            fnt.setBold(false);
            fnt.setStrikeOut(false);
            item->setFont(fnt);
        }

        // preview items affected by active input field
        QString input = uiActiveInput->text().trimmed();
        if (!input.isEmpty()) {
            if (isPatternMatch(item->text(), QStringList() << input)) {
                item->setForeground(QColor(100, 205, 100));
            }
        }
    }
}
