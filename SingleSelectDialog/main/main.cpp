#include <QApplication>
#include <QStringList>
#include "singleselectdialog.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    QStringList list = {"Apple","Orange","Mellon","Kiwi"};
    SingleSelectDialog w;
    w.uiListWidget->addItems(list);
    w.show();

    return a.exec();
}
