#include "singleselectdialog.h"
#include <QVBoxLayout>
#include <QDebug>

SingleSelectDialog::SingleSelectDialog( QWidget *parent, Qt::WindowFlags f) :
    QDialog(parent,f)
{
    init();
}


SingleSelectDialog::~SingleSelectDialog()
{
}

void SingleSelectDialog::init()
{
    resize(400, 400);
    setWindowTitle("Select Dialog");

    uiLineEdit = new QLineEdit(this);
    uiLineEdit->setClearButtonEnabled(true);
    uiLineEdit->setPlaceholderText("Search...");

    uiListWidget = new QListWidget;
    uiListWidget->setSelectionMode(QListWidget::SingleSelection);

    //! setup layout
    QVBoxLayout *layout = new QVBoxLayout(this);
    layout->addWidget(uiLineEdit);
    layout->addWidget(uiListWidget);
    setLayout(layout);

    //! signals-slots
    connect(uiLineEdit, &QLineEdit::textChanged, this, &SingleSelectDialog::filterList);
    connect(uiListWidget, &QListWidget::itemDoubleClicked, this, &SingleSelectDialog::slotDoubleClickedItem);
}

/*!
 * \brief Method used to apply search filter to available items list
 * \param text: Search word used in filtering
 */
void SingleSelectDialog::filterList(const QString text)
{
    if (text.trimmed().isEmpty()){
        for(int i = 0; i < uiListWidget->count(); ++i)
        {
            uiListWidget->item(i)->setHidden(false);
        }
    }else{
        QItemSelectionModel *selection = uiListWidget->selectionModel();
        selection->clearSelection();

        for(int i = 0; i < uiListWidget->count(); ++i)
        {
            if (uiListWidget->item(i)->text().contains(text, Qt::CaseInsensitive)) {
                uiListWidget->item(i)->setHidden(false);
            }else{
                uiListWidget->item(i)->setHidden(true);
            }
        }
    }
}

void SingleSelectDialog::slotDoubleClickedItem()
{
    QList<QListWidgetItem*> selectedItems = uiListWidget->selectedItems();
    if (!selectedItems.isEmpty()) {
        qDebug() << selectedItems.first()->text();
        this->accept();
    }
}
