#ifndef SINGLESELECTDIALOG_H
#define SINGLESELECTDIALOG_H

#include <QDialog>
#include <QListWidget>
#include <QLineEdit>

class SingleSelectDialog : public QDialog
{
    Q_OBJECT

    public:
        explicit SingleSelectDialog(QWidget *parent = nullptr, Qt::WindowFlags f = Qt::WindowFlags());
        ~SingleSelectDialog();

        //! Controls
        QLineEdit *uiLineEdit;
        QListWidget *uiListWidget;

    private:
        void init();
        void filterList(const QString text);

    public slots:
        void slotDoubleClickedItem();
};

#endif // SINGLESELECTDIALOG_H
