#include "colorswatchpicker.h"

#include <QGridLayout>
#include <QWidgetAction>
#include <QApplication>
#include <QClipboard>
#include <QMimeData>
#include <QVariant>

ColorSwatchPicker::ColorSwatchPicker(QWidget *parent) :
    ColorSwatch(parent),
    colorDialog(new QColorDialog(this))
{
    init();
}

ColorSwatchPicker::ColorSwatchPicker(QColor color, QWidget *parent):
    ColorSwatch(parent),
    colorDialog(new QColorDialog(this))
{
    setColor(color);
    init();
}

void ColorSwatchPicker::init()
{
    // setCursor(Qt::PointingHandCursor);
    setContextMenuPolicy(Qt::CustomContextMenu);

    colorDialog->setOption(QColorDialog::ShowAlphaChannel, true);

    createActions();
    createMenus();

    // connections
    connect(this, &QPushButton::clicked, this, &ColorSwatchPicker::slotClicked);
    connect(this, &QPushButton::customContextMenuRequested, this, &ColorSwatchPicker::slotShowContextMenu);
}

void ColorSwatchPicker::createMenus()
{
    //! create color swatch menu
    QWidget *colorWidget = new QWidget(this);
    QGridLayout *layout = new QGridLayout(colorWidget);
    layout->setSpacing(4);
    layout->setContentsMargins(6,6,6,6);

    // create color swatches
    colorList = new QList<QColor>({
            QColor(255,30,10),
            QColor(230,30,100),
            QColor(155,40,175),
            QColor(105,60,185),
            QColor(65,80,180),
            QColor(15,70,255),
            QColor(5,170,245),
            QColor(0,190,210),
            QColor(0,150,135),
            QColor(75,175,80),
            QColor(140,195,75),
            QColor(205,220,60),
            QColor(255,235,60),
            QColor(255,195,5),
            QColor(255,150,0),
            QColor(255,90,35),
            QColor(120,85,70),
            QColor(128,128,128),
            QColor(0,0,0),
            QColor(255,255,255),
        });

    int row = 0;
    int column = 0;

    foreach (const QColor &c, *colorList) {
        auto *cs = new ColorSwatch(c, colorWidget);
        cs->setFixedSize(18,18);
        connect(cs, &ColorSwatch::colorClicked, this, &ColorSwatchPicker::clickedColorSwatch);

        layout->addWidget(cs, row, column);
        column += 1;
        if (column == 5) {
            row += 1;
            column = 0;
        }
    };

    // Quick slider
    uiAlphaSlider = new QSlider(Qt::Horizontal, colorWidget);
    uiAlphaSlider->setRange(0,255);
    uiAlphaSlider->setToolTip("Alpha");
    connect(uiAlphaSlider, &QSlider::valueChanged, this, &ColorSwatchPicker::alphaValueChanged);

    layout->addWidget(uiAlphaSlider, row+1, 0, 1, -1);

    QWidgetAction *colorsAction = new QWidgetAction(this);
    colorsAction->setDefaultWidget(colorWidget);

    // color menu
    colorMenu = new QMenu(tr("&File"), this);
    colorMenu->addAction(colorsAction);
    colorMenu->addAction(this->colorPickerAct);

    // context menu
    contextMenu = new QMenu(tr("&Edit"), this);
    contextMenu->addAction(copyAct);
    contextMenu->addAction(pasteAct);
}

void ColorSwatchPicker::createActions()
{
    colorPickerAct = new QAction(tr("&Show Color Picker"), this);
    colorPickerAct->setStatusTip(tr("Show Color Picker"));
    connect(colorPickerAct, &QAction::triggered, this, &ColorSwatchPicker::slotShowColorDialog);

    copyAct = new QAction(tr("&Copy"), this);
    copyAct->setStatusTip(tr("Copy Color"));
    connect(copyAct, &QAction::triggered, this, &ColorSwatchPicker::slotCopyColor);

    pasteAct = new QAction(tr("&Paste"), this);
    pasteAct->setStatusTip(tr("Paste Color"));
    connect(pasteAct, &QAction::triggered, this, &ColorSwatchPicker::slotPasteColor);
}

void ColorSwatchPicker::slotShowContextMenu()
{
    QClipboard *clip = QApplication::clipboard();
    if (clip->mimeData()->hasColor()) {
        this->pasteAct->setEnabled(true);
    }else{
        this->pasteAct->setEnabled(false);
    }

    contextMenu->exec(QCursor::pos());
}

void ColorSwatchPicker::slotClicked()
{
    uiAlphaSlider->setValue(this->color().alpha());
    colorMenu->exec(QCursor::pos());
}

void ColorSwatchPicker::slotCopyColor()
{
    QMimeData *mime = new QMimeData;
    mime->setColorData(this->color());

    QClipboard *clip = QApplication::clipboard();
    clip->setMimeData(mime);
}

void ColorSwatchPicker::slotPasteColor()
{
     QClipboard *clip = QApplication::clipboard();
     if (clip->mimeData()->hasColor()) {
         this->setColor(qvariant_cast<QColor>(clip->mimeData()->colorData()));
     }
}

void ColorSwatchPicker::slotShowColorDialog()
{
    this->colorDialog->setCurrentColor(this->color());
    if(this->colorDialog->exec())
        this->setColor(this->colorDialog->currentColor());
}

void ColorSwatchPicker::clickedColorSwatch()
{
    ColorSwatch *colorSwatch= qobject_cast<ColorSwatch *>(sender());
    if (colorSwatch){
        this->setColor(colorSwatch->color());
        this->update();
        colorMenu->close();
    }
}

void ColorSwatchPicker::alphaValueChanged()
{
    this->setAlpha(uiAlphaSlider->value());
}
