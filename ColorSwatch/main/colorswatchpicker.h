#ifndef COLORSWATCHPICKER_H
#define COLORSWATCHPICKER_H

#include <QWidget>
#include <colorswatch.h>
#include <QColorDialog>
#include <QMenu>
#include <QList>
#include <QSlider>
#include <QAction>

class ColorSwatchPicker : public ColorSwatch
{
    Q_OBJECT
public:
    ColorSwatchPicker(QWidget *parent = nullptr);
    ColorSwatchPicker(QColor color, QWidget *parent);

    QColorDialog *colorDialog;
    QSlider *uiAlphaSlider;
    QMenu *colorMenu;
    QMenu *contextMenu;
    QAction *copyAct;
    QAction *pasteAct;
    QAction *colorPickerAct;

signals:

private:
    void init();
    void slotShowContextMenu();
    void createMenus();
    void createActions();
    void slotClicked();
    void slotCopyColor();
    void slotPasteColor();
    void slotShowColorDialog();
    void clickedColorSwatch();
    void alphaValueChanged();

    QList<QColor> *colorList;
};

#endif // COLORSWATCHPICKER_H
