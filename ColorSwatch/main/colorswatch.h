#ifndef COLORSWATCH_H
#define COLORSWATCH_H

#include <QWidget>
#include <QPushButton>

class ColorSwatch : public QPushButton
{
    Q_OBJECT
    Q_PROPERTY(QColor color READ color WRITE setColor NOTIFY colorChanged)

    QColor m_color;

public:
    ColorSwatch(QWidget *parent = 0);
    ColorSwatch(QColor color, QWidget *parent);

    void setColor(const QColor &color);
    void setAlpha(const int &value);
    QColor color() const;

    void init();

signals:
    void colorChanged(QColor color);
    void colorClicked(QColor color);

protected:
    void paintEvent(QPaintEvent *event) override;
    void mousePressEvent(QMouseEvent *event) override;
};

#endif // COLORSWATCH_H
