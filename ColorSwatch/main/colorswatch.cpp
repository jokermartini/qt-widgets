#include "colorswatch.h"
#include <QPainter>
#include <QDebug>


//! Constructors
ColorSwatch::ColorSwatch(QWidget *parent) :
    QPushButton(parent),
    m_color(QColor(0,0,0))
{
    init();
}

ColorSwatch::ColorSwatch(QColor color, QWidget *parent) :
    QPushButton(parent),
    m_color(QColor(0,0,0))
{
    m_color = color;
    init();
}

void ColorSwatch::init() {
    setMinimumSize(6,6);
    setToolTip( QString::number(m_color.red()) + ", "
                + QString::number(m_color.green()) + ", "
                + QString::number(m_color.blue()) + ", "
                + QString::number(m_color.alpha()));
}

//! Methods
void ColorSwatch::setColor(const QColor &color)
{
    setToolTip( QString::number(color.red()) + ", "
                + QString::number(color.green()) + ", "
                + QString::number(color.blue()) + ", "
                + QString::number(color.alpha()));
    if (m_color == color)
        return;

    m_color = color;
    emit colorChanged(m_color);
    this->update();
}

void ColorSwatch::setAlpha(const int &value)
{
    m_color.setAlpha(value);
    emit colorChanged(m_color);
    this->update();
}

QColor ColorSwatch::color() const
{
    return m_color;
}

void ColorSwatch::paintEvent(QPaintEvent *event)
{
    QPainter painter(this);
    //painter.setPen(Qt::NoPen);
    //painter.setBrush(QBrush(QColor(0,0,0), Qt::SolidPattern));
    //painter.drawRoundedRect(0, 0, width(), height(), 2, 2);
    painter.fillRect(0, 0, width(), height(), QColor(0,0,0,255));

    // Create tiled background to show opacity
    QPixmap px(4, 4);
    px.fill(Qt::transparent);
    QPainter pattern_painter(&px);

    pattern_painter.setPen(Qt::NoPen);
    pattern_painter.setBrush(QBrush(QColor(255,255,255), Qt::SolidPattern));
    pattern_painter.drawRect(0, 0, 2, 2);
    pattern_painter.drawRect(2, 2, 2, 2);
    painter.drawTiledPixmap(1, 1, width()-3, height()-3, px);

    // Paint actual color swatch supporting opacity
    painter.setBrush(m_color);
    painter.setPen(QPen(QColor(255,255,255), 1, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
    painter.drawRect(1,1,width()-3, height()-3);
}

void ColorSwatch::mousePressEvent(QMouseEvent *event)
{
    emit colorClicked(m_color);
    QPushButton::mousePressEvent(event);
}

