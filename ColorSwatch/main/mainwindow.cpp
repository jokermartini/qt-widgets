#include "mainwindow.h"
#include "colorswatch.h"
#include "colorswatchpicker.h"
#include <QVBoxLayout>


MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
{
    auto *widget = new QWidget();
    auto *layout = new QVBoxLayout(widget);
    auto *colorSwatchA = new ColorSwatchPicker(this);

    layout->addWidget(colorSwatchA);
    layout->addStretch();
    widget->setLayout(layout);
    setCentralWidget(widget);
    resize(200,200);
}

MainWindow::~MainWindow()
{

}
