#include "tokenaddeditdialog.h"

#include <QPushButton>
#include <QRegExp>
#include <QRegExpValidator>

TokenAddEditDialog::TokenAddEditDialog(QString title, QWidget *parent) :
    QDialog(parent)
{
    setWindowTitle(title);

    init();
}

TokenAddEditDialog::TokenAddEditDialog(QString title, QString key, QString value, QWidget *parent) :
    QDialog(parent)
{
    setWindowTitle(title);

    init();

    uiKey->setText(key);
    uiValue->setText(value);
}

void TokenAddEditDialog::init()
{
    resize(400, 100);

    QRegExp regex("[A-Za-z0-9_]+");
    QRegExpValidator *validator = new QRegExpValidator(regex, this);

    uiKey = new QLineEdit(this);
    uiKey->setClearButtonEnabled(true);
    uiKey->setValidator(validator);

    uiValue = new QLineEdit(this);
    uiValue->setClearButtonEnabled(true);

    buttonBox = new QDialogButtonBox(this);
    buttonBox->setOrientation(Qt::Horizontal);
    buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);

    // layout
    layout = new QFormLayout(this);
    layout->addRow("Key", uiKey);
    layout->addRow("Value", uiValue);
    layout->addWidget(buttonBox);
    setLayout(layout);

    //signals-slots connects
    connect(buttonBox, SIGNAL(accepted()), this, SLOT(accept()));
    connect(buttonBox, SIGNAL(rejected()), this, SLOT(reject()));
    connect(uiKey, &QLineEdit::textChanged, this, &TokenAddEditDialog::updateControls);

    // start
    updateControls();
}

void TokenAddEditDialog::updateControls()
{
    // key is required
    if (uiKey->text().trimmed().isEmpty()) {
        buttonBox->button(QDialogButtonBox::Ok)->setEnabled(false);
    } else {
        buttonBox->button(QDialogButtonBox::Ok)->setEnabled(true);
    }
}

TokenAddEditDialog::~TokenAddEditDialog()
{
}
