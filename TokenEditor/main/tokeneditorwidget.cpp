#include "tokeneditorwidget.h"
#include "tokenaddeditdialog.h"

#include <QDebug>

#include <QHeaderView>
#include <QStandardItem>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QFileDialog>
#include <QFile>
#include <QMessageBox>

TokenEditorWidget::TokenEditorWidget(QWidget *parent) : QWidget(parent)
{
    init();
}

void TokenEditorWidget::init()
{
    resize(400, 600);
    setWindowTitle("Token Editor");

    // global table
    globalModel = new QStandardItemModel(this);
    globalModel->setHorizontalHeaderLabels(QStringList{"Key", "Value"});

    uiGlobalTable = new QTableView(this);
    uiGlobalTable->setModel(globalModel);
    uiGlobalTable->setSortingEnabled(true);
    uiGlobalTable->setAlternatingRowColors(true);
    uiGlobalTable->setShowGrid(false);
    uiGlobalTable->setEditTriggers(QAbstractItemView::NoEditTriggers);
    uiGlobalTable->setSelectionBehavior(QAbstractItemView::SelectRows);
    uiGlobalTable->setContextMenuPolicy(Qt::CustomContextMenu);
    uiGlobalTable->verticalHeader()->setVisible(false);
    uiGlobalTable->setSortingEnabled(true);

    // custom table
    customModel = new QStandardItemModel(this);
    customModel->setHorizontalHeaderLabels(QStringList{"Key", "Value"});

    uiCustomTable = new QTableView(this);
    uiCustomTable->setModel(customModel);
    uiCustomTable->setSortingEnabled(true);
    uiCustomTable->setAlternatingRowColors(true);
    uiCustomTable->setShowGrid(false);
    uiCustomTable->setEditTriggers(QAbstractItemView::NoEditTriggers);
    uiCustomTable->setSelectionBehavior(QAbstractItemView::SelectRows);
    uiCustomTable->setContextMenuPolicy(Qt::CustomContextMenu);
    uiCustomTable->verticalHeader()->setVisible(false);
    uiCustomTable->setSortingEnabled(true);

    // ToolBar
    toolBar = new QToolBar(this);

    // layout
    layout = new QVBoxLayout(this);
    layout->setContentsMargins(0,0,0,0);
    layout->setSpacing(5);
    layout->addWidget(uiGlobalTable);
    layout->addWidget(toolBar);
    layout->addWidget(uiCustomTable);
    setLayout(layout);

    // actions/menus
    createActions();
    createMenus();
    createToolBars();

    // signals/slots
    connect(uiCustomTable, &QTableView::customContextMenuRequested, this, &TokenEditorWidget::slotContextMenu);
    connect(uiCustomTable, &QTableView::doubleClicked, this, &TokenEditorWidget::slotEditToken);
    connect(uiCustomTable->selectionModel(), &QItemSelectionModel::selectionChanged, this, &TokenEditorWidget::slotSelectionChanged);

    updateControls();
}

void TokenEditorWidget::createActions()
{
    newTokenAct = new QAction(tr("&New..."), this);
    connect(newTokenAct, &QAction::triggered, this, &TokenEditorWidget::slotNewToken);

    editTokenAct = new QAction(tr("&Edit..."), this);
    connect(editTokenAct, &QAction::triggered, this, &TokenEditorWidget::slotEditToken);

    removeTokensAct = new QAction(tr("&Remove"), this);
    connect(removeTokensAct, &QAction::triggered, this, &TokenEditorWidget::slotRemoveSelectedTokens);

    clearTokensAct = new QAction(tr("&Clear"), this);
    connect(clearTokensAct, &QAction::triggered, this, [=](){TokenEditorWidget::slotClearTokens();});

    saveTokensAct = new QAction(tr("&Save..."), this);
    connect(saveTokensAct, &QAction::triggered, this, [=](){TokenEditorWidget::slotSaveTokens();});

    loadTokensAct = new QAction(tr("&Load..."), this);
    connect(loadTokensAct, &QAction::triggered, this, [=](){TokenEditorWidget::slotLoadTokens();});
}

void TokenEditorWidget::createMenus()
{
    // menu bar
    fileMenu = new QMenu("File", this);

    fileMenu->addAction(newTokenAct);
    fileMenu->addSeparator();
    fileMenu->addAction(saveTokensAct);
    fileMenu->addAction(loadTokensAct);

    menuBar = new QMenuBar(this);
    menuBar->addMenu(fileMenu);
    layout->setMenuBar(menuBar);

    // context menu
    contextMenu = new QMenu("Edit", this);
    contextMenu->addAction(newTokenAct);
    contextMenu->addAction(editTokenAct);
    contextMenu->addSeparator();
    contextMenu->addAction(removeTokensAct);
    contextMenu->addSeparator();
    contextMenu->addAction(clearTokensAct);
}

void TokenEditorWidget::createToolBars()
{
    toolBar->addAction(newTokenAct);
    toolBar->addAction(editTokenAct);
    toolBar->addSeparator();
    toolBar->addAction(saveTokensAct);
    toolBar->addAction(loadTokensAct);
    toolBar->addSeparator();
    toolBar->addAction(removeTokensAct);
    toolBar->addSeparator();
    toolBar->addAction(clearTokensAct);
}

void TokenEditorWidget::updateControls()
{
    int count = uiCustomTable->selectionModel()->selectedRows(0).count();

    editTokenAct->setEnabled(count == 1);
    removeTokensAct->setEnabled(count >= 1);
}

void TokenEditorWidget::slotClearTokens(TokenType type)
{
    QMessageBox::StandardButton reply;
    reply = QMessageBox::question(this, "Token Editor", "Are you sure you want to delete the selected tokens?", QMessageBox::Yes|QMessageBox::No);
    if (reply == QMessageBox::No) {
        return;
    }
    switch(type) {
        case TokenType::Global:
            globalModel->removeRows(0, globalModel->rowCount());
            break;
        case TokenType::Custom:
            customModel->removeRows(0, customModel->rowCount());
            break;
        default:
            return;
    }
}

void TokenEditorWidget::setTokens(QMap<QString, QString> tokens, TokenType type)
{
    QStandardItemModel *model;
    QTableView *table;

    switch(type) {
        case TokenType::Global:
            model = globalModel;
            table = uiGlobalTable;
            break;
        case TokenType::Custom:
            model = customModel;
            table = uiCustomTable;
            break;
        default:
            return;
    }

    model->removeRows(0, model->rowCount());

    foreach(QString key, tokens.keys())
    {
        QList<QStandardItem*> rowData;
        rowData << new QStandardItem(key);
        rowData << new QStandardItem(tokens[key]);
        model->appendRow(rowData);
    }

    table->resizeColumnsToContents();
    table->sortByColumn(0, Qt::AscendingOrder);
    table->horizontalHeader()->setStretchLastSection(true);
}

QMap<QString, QString> TokenEditorWidget::getTokens(TokenEditorWidget::TokenType type)
{
    QMap<QString, QString> map;

    QStandardItemModel *model;

    switch(type) {
        case TokenType::Global:
            model = globalModel;
            break;
        case TokenType::Custom:
            model = customModel;
            break;
        default:
            return map;
    }

    for(int i=0; i < model->rowCount(); i++)
    {
        QString key = model->index(i, 0).data().toString();
        QString value = model->index(i, 1).data().toString();
        map[key] = value;
    }

    return map;
}

void TokenEditorWidget::slotContextMenu()
{
    if (uiCustomTable->selectionModel()->hasSelection()) {
        editTokenAct->setEnabled(true);
        removeTokensAct->setEnabled(true);
    } else {
        editTokenAct->setEnabled(false);
        removeTokensAct->setEnabled(false);
    }
    contextMenu->exec(QCursor::pos());
}

void TokenEditorWidget::slotRemoveSelectedTokens()
{
    QMessageBox::StandardButton reply;
    reply = QMessageBox::question(this, "Token Editor", "Are you sure you want to delete the selected tokens?", QMessageBox::Yes|QMessageBox::No);
    if (reply == QMessageBox::Yes) {
        QModelIndexList indexes;
        while((indexes = uiCustomTable->selectionModel()->selectedIndexes()).size()) {
            uiCustomTable->model()->removeRow(indexes.first().row());
        }
    }
}

void TokenEditorWidget::slotSaveTokens(QString filepath)
{
    if (filepath.isEmpty())
        filepath = QFileDialog::getSaveFileName(this, "Save", "", "Vexus Tokens (*.vxt)");

    if (filepath.isEmpty())
        return;

    QFile file(filepath);
    if(file.open(QIODevice::WriteOnly))
    {
        QJsonArray array;
        for(int i=0; i < uiCustomTable->model()->rowCount(); i++)
        {
            QJsonObject obj;
            obj["key"] = uiCustomTable->model()->index(i, 0).data().toString();
            obj["value"] = uiCustomTable->model()->index(i, 1).data().toString();
            array.push_back(QJsonValue(obj));
        }
        QJsonDocument doc(array);
        auto data = doc.toJson(QJsonDocument::Indented);
        file.write(data);
        file.close();
    }
}

void TokenEditorWidget::slotLoadTokens(QString filepath)
{
    if (filepath.isEmpty())
        filepath = QFileDialog::getOpenFileName(this, "Open", "", "Vexus Tokens (*.vxt)");

    customModel->removeRows(0, customModel->rowCount());

    QFile file(filepath);
    if(file.open(QIODevice::ReadOnly))
    {
        auto data = file.readAll();
        QJsonDocument doc = QJsonDocument::fromJson(data);
        auto array = doc.array();
        for(int i=0; i<array.size(); i++)
        {
            auto object = array[i].toObject();
            QString key = object.value("key").toString();
            QString value = object.value("value").toString();

            QList<QStandardItem*> rowData;
            rowData << new QStandardItem(key);
            rowData << new QStandardItem(value);
            customModel->appendRow(rowData);
        }
        file.close();
    }
}

void TokenEditorWidget::slotAppendToken(QString key, QString value, TokenType type)
{
    QStandardItemModel *model;
    QTableView *table;

    switch(type) {
        case TokenType::Global:
            model = globalModel;
            table = uiGlobalTable;
            break;
        case TokenType::Custom:
            model = customModel;
            table = uiCustomTable;
            break;
        default:
            return;
    }

    // Add new token
    QList<QStandardItem*> rowData;
    rowData << new QStandardItem(key);
    rowData << new QStandardItem(value);
    model->appendRow(rowData);

    table->resizeColumnsToContents();
    table->sortByColumn(0, Qt::AscendingOrder);
    table->horizontalHeader()->setStretchLastSection(true);
}

void TokenEditorWidget::slotNewToken()
{
    TokenAddEditDialog *dialog = new TokenAddEditDialog("New Token", this);
    if (dialog->exec()) {
        slotAppendToken(dialog->uiKey->text(), dialog->uiValue->text(), TokenType::Custom);
    }
}

void TokenEditorWidget::slotEditToken()
{
    if (uiCustomTable->selectionModel()->hasSelection()) {
        QModelIndex index = uiCustomTable->selectionModel()->currentIndex();

        QString key = customModel->data(customModel->index(index.row(), 0)).toString();
        QString value = customModel->data(customModel->index(index.row(), 1)).toString();

        uiCustomTable->setSortingEnabled(false);
        TokenAddEditDialog *dialog = new TokenAddEditDialog("Edit Token", key, value, this);
        dialog->uiValue->selectAll();
        dialog->uiValue->setFocus();
        if (dialog->exec()) {
            customModel->setData(customModel->index(index.row(), 0), dialog->uiKey->text());
            customModel->setData(customModel->index(index.row(), 1), dialog->uiValue->text());
        }
        uiCustomTable->setSortingEnabled(true);
    }
}

void TokenEditorWidget::slotSelectionChanged(const QItemSelection &selected, const QItemSelection &deselected)
{
    Q_UNUSED(selected);
    Q_UNUSED(deselected);

    updateControls();
}
