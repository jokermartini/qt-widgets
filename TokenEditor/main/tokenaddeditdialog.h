#ifndef TOKENADDEDITDIALOG_H
#define TOKENADDEDITDIALOG_H

#include <QDialog>
#include <QLineEdit>
#include <QFormLayout>
#include <QDialogButtonBox>

class TokenAddEditDialog : public QDialog
{
    Q_OBJECT

    public:
        explicit TokenAddEditDialog(QString title, QWidget *parent = nullptr);
        explicit TokenAddEditDialog(QString title, QString key, QString value,
                                        QWidget *parent = nullptr);
        ~TokenAddEditDialog();

        QLineEdit *uiKey;
        QLineEdit *uiValue;
        QDialogButtonBox *buttonBox;
        QFormLayout *layout;

    private:
        void init();
        void updateControls();
};

#endif // TOKENADDEDITDIALOG_H
