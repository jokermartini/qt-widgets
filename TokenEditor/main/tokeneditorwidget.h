#ifndef TOKENEDITORWIDGET_H
#define TOKENEDITORWIDGET_H

#include <QWidget>
#include <QTableView>
#include <QStandardItemModel>
#include <QAction>
#include <QMenu>
#include <QMenuBar>
#include <QVBoxLayout>
#include <QToolBar>

class TokenEditorWidget : public QWidget
{
    Q_OBJECT

    public:
        explicit TokenEditorWidget(QWidget *parent = nullptr);

        enum TokenType {
                Global,
                Custom,
                All
            };

        QVBoxLayout *layout;
        QTableView *uiGlobalTable;
        QStandardItemModel *globalModel;

        QTableView *uiCustomTable;
        QStandardItemModel *customModel;

        QAction *newTokenAct;
        QAction *editTokenAct;
        QAction *removeTokensAct;
        QAction *clearTokensAct;
        QAction *saveTokensAct;
        QAction *loadTokensAct;
        QMenu *fileMenu;
        QMenu *contextMenu;
        QMenuBar *menuBar;
        QToolBar *toolBar;

        void setTokens(QMap<QString, QString> tokens, TokenType type);
        QMap<QString, QString> getTokens(TokenType type);

    private:
        void init();
        void createActions();
        void createMenus();
        void createToolBars();
        void updateControls();

    public slots:
        void slotClearTokens(TokenType type=TokenType::Custom);
        void slotContextMenu();
        void slotRemoveSelectedTokens();
        void slotSaveTokens(QString filepath="");
        void slotLoadTokens(QString filepath="");
        void slotAppendToken(QString key, QString value, TokenType type);
        void slotNewToken();
        void slotEditToken();
        void slotSelectionChanged(const QItemSelection & selected, const QItemSelection & deselected);
};

#endif // TOKENEDITORWIDGET_H
