#include "tokeneditorwidget.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    TokenEditorWidget w;
    w.setTokens({
          {"SHOT", "Shot node name"},
          {"PASS", "Pass node name"},
          {"SELF", "Current node name"},
          {"MM", "Month as integer with leading zeros"},
          {"DD", "Day as integer with leading zeros"},
          {"YY", "Year as two digit integer"},
          {"YYYY", "Year as four digit integer"},
          {"HH", "Hours as integer with leading zeros"},
          {"MN", "Minutes as integer with leading zeros"},
          {"SS", "Seconds as integer with leading zeros"},
          {"USER", "Windows user name"},
          {"MACHINE", "Machine name"},
          {"MAXFILENAME", "Max file name without the extension"},
          {"MAXFILEPATH", "Max file directory location"},
          {"MAXPROJPATH", "Max project directory path"},
        }, TokenEditorWidget::TokenType::Global);

    w.setTokens({
        {"Mine", "A"},
        {"Studio", "B"},
        {"House", "C"},
        {"Car", "D"},
        {"Boat", "E"},
        {"Pet", "F"},
        }, TokenEditorWidget::TokenType::Custom);

    //auto values = w.getTokens(TokenEditorWidget::TokenType::Custom);
    w.show();

    return a.exec();
}
