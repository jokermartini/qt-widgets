#include "mainwindow.h"
#include "buttonextended.h"
#include <QVBoxLayout>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
{
    auto *widget = new QWidget(this);
    auto *lay = new QVBoxLayout(widget);
    auto *ctrl = new ButtonExtended(this);

    lay->addWidget(ctrl);
    lay->addStretch();
    setCentralWidget(widget);
}

MainWindow::~MainWindow()
{

}
