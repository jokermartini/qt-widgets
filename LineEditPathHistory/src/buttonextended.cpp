#include "buttonextended.h"
#include <QHBoxLayout>
#include <QProcess>
#include <QClipboard>
#include <QApplication>
#include <QSettings>

ButtonExtended::ButtonExtended(QWidget *parent) :
    QWidget(parent),
    uiButton(new QPushButton("...", this)),
    uiLineEdit(new QLineEdit(this)),
    uiFileDialog(new QFileDialog(this))
{
    // setup browser button
    uiButton->setFixedSize(22,22);
    uiButton->setText("...");
    uiButton->setContextMenuPolicy(Qt::CustomContextMenu);

    // setup file dialog
    uiFileDialog->setAcceptMode(QFileDialog::AcceptOpen);
    uiFileDialog->setFileMode(QFileDialog::ExistingFile);

    // setup layout
    QHBoxLayout *layout = new QHBoxLayout(this);
    layout->setContentsMargins(QMargins());
    layout->setSpacing(0);
    layout->addWidget(uiLineEdit);
    layout->addWidget(uiButton);
    setLayout(layout);

    createActions();
    createMenus();

    // connections
    connect(uiButton, &QPushButton::clicked, this, &ButtonExtended::showDialog);
    connect(uiButton, &QPushButton::customContextMenuRequested, this, &ButtonExtended::showContextMenu);
    connect(uiFileDialog, &QFileDialog::fileSelected, this, &ButtonExtended::dialogFileSelected);
}

void ButtonExtended::dialogFileSelected(const QString &file)
{
    // file has been selected
    uiLineEdit->setText(file);
    appendRecentFile(file);
}

void ButtonExtended::showInExplorer()
{
    QString path = uiLineEdit->text();
    QStringList args;
    args << "/select," << QDir::toNativeSeparators(path);
    QProcess *process = new QProcess(this);
    process->start("explorer.exe", args);
}

void ButtonExtended::showContextMenu()
{
    createRecentFileMenu();
    fileMenu->exec(QCursor::pos());
}

void ButtonExtended::copyToClipboard()
{
    QClipboard *clip = QApplication::clipboard();
    QString input = uiLineEdit->text();
    clip->setText(input);
}

void ButtonExtended::createActions()
{
    showInExplorerAct = new QAction(tr("&Show In Explorer"), this);
    showInExplorerAct->setShortcut(tr("Ctrl+Shift+S"));
    showInExplorerAct->setStatusTip(tr("Shows path in explorer"));
    connect(showInExplorerAct, &QAction::triggered, this, &ButtonExtended::showInExplorer);

    copyToClipboardAct = new QAction(tr("&Copy To Clipboard"), this);
    copyToClipboardAct->setStatusTip(tr("Copies path to clipboard"));
    connect(copyToClipboardAct, &QAction::triggered, this, &ButtonExtended::copyToClipboard);

    // recent menu actions
    for (int i = 0; i < MaxRecentFiles; ++i) {
        recentFileActs[i] = new QAction(this);
        recentFileActs[i]->setVisible(false);
        connect(recentFileActs[i], &QAction::triggered, this, &ButtonExtended::clickedRecentFile);
    }
}

void ButtonExtended::createMenus()
{
    fileMenu = new QMenu(tr("&File"), this);
    fileMenu->addAction(showInExplorerAct);
    fileMenu->addAction(copyToClipboardAct);
    recentFilesMenu = fileMenu->addMenu(tr("&Recent Files"));
    for (int i = 0; i < MaxRecentFiles; ++i)
    {
        recentFilesMenu->addAction(recentFileActs[i]);
    }
    createRecentFileMenu();
}

void ButtonExtended::showDialog()
{
    if(uiFileDialog->isVisible()) {
        uiFileDialog->raise();
        uiFileDialog->activateWindow();
        return;
    }
    uiFileDialog->open();
}

void ButtonExtended::clickedRecentFile()
{
    QAction *action = qobject_cast<QAction *>(sender());
    if (action)
        uiLineEdit->setText(action->data().toString());
}

void ButtonExtended::appendRecentFile(const QString &path)
{
    QSettings settings("JokerMartini", "Application");
    QStringList files = settings.value("recentFileList").toStringList();
    files.removeAll(path);
    files.prepend(path);
    while (files.size() > MaxRecentFiles)
        files.removeLast();
    settings.setValue("recentFileList", files);
}

void ButtonExtended::createRecentFileMenu()
{
    QSettings settings("JokerMartini", "Application");
    QStringList files = settings.value("recentFileList").toStringList();

    int numRecentFiles = qMin(files.size(), (int)MaxRecentFiles);

    for (int i = 0; i < numRecentFiles; ++i) {
        recentFileActs[i]->setText(files[i]);
        recentFileActs[i]->setData(files[i]);
        recentFileActs[i]->setVisible(true);
    }
//    for (int j = numRecentFiles; j < MaxRecentFiles; ++j)
//        recentFileActs[j]->setVisible(false);
}
