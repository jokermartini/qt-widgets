#ifndef BUTTONEXTENDED_H
#define BUTTONEXTENDED_H

#include <QWidget>
#include <QPushButton>
#include <QFileDialog>
#include <QLineEdit>
#include <QMenu>
#include <QAction>

class ButtonExtended : public QWidget
{
    Q_OBJECT
public:
    explicit ButtonExtended(QWidget *parent = nullptr);

    QLineEdit *uiLineEdit;
    QPushButton *uiButton;
    QFileDialog *uiFileDialog;

public slots:
    void dialogFileSelected(const QString &file);
    void showInExplorer();
    void showDialog();
    void showContextMenu();
    void copyToClipboard();
    void appendRecentFile(const QString &file);

private slots:
    void clickedRecentFile();

private:
    void createActions();
    void createMenus();
    void createRecentFileMenu();

    QMenu *fileMenu;
    QMenu *recentFilesMenu;
    QAction *showInExplorerAct;
    QAction *copyToClipboardAct;

    enum { MaxRecentFiles = 10 };
    QAction *recentFileActs[MaxRecentFiles];
};

#endif // BUTTONEXTENDED_H
