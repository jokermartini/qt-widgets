#include "collapsiblepanel.h"
#include <QDebug>
#include <QApplication>


CollapsiblePanel::CollapsiblePanel(QWidget *parent) :
    QWidget(parent)
{
    initialize();
}

CollapsiblePanel::CollapsiblePanel(const QString &text, QWidget *parent) :
    QWidget(parent)
{
    initialize();
    setTitle(text);
}

CollapsiblePanel::CollapsiblePanel(const QString &text, const bool isExpanded, QWidget *parent) :
    QWidget(parent)
{
    initialize();
    setTitle(text);
    setExpanded(isExpanded);
}

void CollapsiblePanel::initialize()
{
    // controls
    button = new QPushButton();
    button->setSizePolicy(QSizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred));
    button->setContextMenuPolicy(Qt::CustomContextMenu);
    button->setCursor(Qt::PointingHandCursor);

    checkbox = new QCheckBox();
    checkbox->setChecked(true);
    checkbox->setSizePolicy(QSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed));
    checkbox->setAttribute(Qt::WA_TransparentForMouseEvents);

    label = new QLabel();
    label->setSizePolicy(QSizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred));
    label->setAttribute(Qt::WA_TransparentForMouseEvents);

    widget = new QWidget();

    // layout
    layout = new QGridLayout(this);
    layout->setContentsMargins(0, 0, 0, 0);
    layout->setHorizontalSpacing(0);
    layout->setVerticalSpacing(0);
    layout->setSpacing(0);
    layout->addWidget(button, 0, 0, 1, -1);
    layout->addWidget(checkbox, 0, 0);
    layout->addWidget(label, 0, 1, 1, -1);
    layout->addWidget(widget, 1, 0, 1, -1);

    this->setSizePolicy(QSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed));

    // Menus/Actions
    createActions();
    createMenus();

    // connections
    connect(checkbox, SIGNAL(stateChanged(int)), this, SLOT(slotToggleChanged()));
    connect(button, SIGNAL(clicked()), this, SLOT(slotToggleClicked()));
    connect(button, SIGNAL(customContextMenuRequested(QPoint)), this, SLOT(slotShowContextMenu()));
}

void CollapsiblePanel::setTitle(const QString text)
{
    label->setText(text);
}

void CollapsiblePanel::setExpanded(const bool value)
{
    checkbox->blockSignals(true);
    if (value) {
        // expanded
        checkbox->setChecked(true);
        widget->setVisible(true);
        emit expanded();
    }else{
        // collapsed
        checkbox->setChecked(false);
        widget->setVisible(false);
        emit collapsed();
    }
    checkbox->blockSignals(false);
}

void CollapsiblePanel::setPanelLayout(QLayout *layout)
{
    widget->setLayout(layout);
}

void CollapsiblePanel::collapseAll()
{
    QList<CollapsiblePanel*> list = this->parent()->findChildren<CollapsiblePanel*>();
    foreach(CollapsiblePanel *w, list) {
        w->setExpanded(false);
    }
}

void CollapsiblePanel::expandAll()
{
    QList<CollapsiblePanel*> list = this->parent()->findChildren<CollapsiblePanel*>();
    foreach(CollapsiblePanel *w, list) {
        w->setExpanded(true);
    }
}

void CollapsiblePanel::slotToggleClicked()
{
    checkbox->setChecked(!checkbox->isChecked());
    emit clicked();

    // If modifier Ctrl then trigger neighboring collapsible's
    auto modifiers = QApplication::keyboardModifiers();
    if (modifiers == Qt::ControlModifier) {
        if (checkbox->isChecked()){
            expandAll();
        }else{
            collapseAll();
        }
    }
}

void CollapsiblePanel::slotToggleChanged()
{
    setExpanded(checkbox->isChecked());
}

void CollapsiblePanel::slotShowContextMenu()
{
    contextMenu->exec(QCursor::pos());
}

void CollapsiblePanel::createActions()
{
    collapseAct = new QAction(tr("&Collapse All"), this);
    collapseAct->setStatusTip(tr("Collapses All Collapsible Panels"));
    connect(collapseAct, &QAction::triggered, this, &CollapsiblePanel::collapseAll);

    expandAct = new QAction(tr("&Expand All"), this);
    expandAct->setStatusTip(tr("Expands All Collapsible Panels"));
    connect(expandAct, &QAction::triggered, this, &CollapsiblePanel::expandAll);
}

void CollapsiblePanel::createMenus()
{
    contextMenu = new QMenu(tr("&File"), this);
    contextMenu->addAction(collapseAct);
    contextMenu->addAction(expandAct);
}
