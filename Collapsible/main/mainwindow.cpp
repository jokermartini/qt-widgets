#include "mainwindow.h"
#include "collapsiblepanel.h"
#include <QVBoxLayout>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
{
    QWidget *widget = new QWidget();
    QVBoxLayout *layout = new QVBoxLayout(widget);
    layout->setContentsMargins(0,0,0,0);
    layout->setSpacing(0);

    auto *ctrlA = new CollapsiblePanel(this);
    auto *ctrlB = new CollapsiblePanel("Rollout", false, this);
    auto *ctrlC = new CollapsiblePanel("Parameters", this);

    auto *layA = new QVBoxLayout(ctrlA->widget);
    auto *btnA = new QPushButton("Button A");
    layA->addWidget(btnA);
    ctrlA->setPanelLayout(layA);

    auto *layB = new QVBoxLayout(ctrlB->widget);
    auto *btnB = new QPushButton("Button B");
    layB->addWidget(btnB);
    ctrlB->setPanelLayout(layB);

    auto *layC = new QVBoxLayout(ctrlC->widget);
    auto *btnC = new QPushButton("Button C");
    layC->addWidget(btnC);
    ctrlC->setPanelLayout(layC);

    layout->addWidget(ctrlA);
    layout->addWidget(ctrlB);
    layout->addWidget(ctrlC);
    layout->addStretch();
    widget->setLayout(layout);
    setCentralWidget(widget);
    resize(200,200);

    this->setStyleSheet(
        "CollapsiblePanel > QPushButton {background:rgb(40,40,40); border:none; border-bottom: 1px solid rgb(70,70,70);}"
        "CollapsiblePanel > QLabel {color:rgb(240,240,240); border:none; background:transparent; qproperty-alignment: 'AlignVCenter | AlignLeft';}"
        "CollapsiblePanel > QCheckBox {padding: 4px;}"
        "CollapsiblePanel > QCheckBox::indicator {width:12px; height:12px; background:rgb(50,50,50); border:1px solid rgb(40,40,40); border-radius:2px;}"
        "CollapsiblePanel > QCheckBox::indicator:checked {background:rgb(80,80,80);}"
    );
}

MainWindow::~MainWindow()
{

}
