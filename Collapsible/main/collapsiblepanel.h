#ifndef COLLAPSIBLEPANEL_H
#define COLLAPSIBLEPANEL_H

#include <QWidget>
#include <QPushButton>
#include <QCheckBox>
#include <QLabel>
#include <QGridLayout>
#include <QMenu>
#include <QAction>


class CollapsiblePanel : public QWidget
{
        Q_OBJECT

    public:
        CollapsiblePanel(QWidget *parent = nullptr);
        CollapsiblePanel(const QString &text, QWidget *parent = nullptr);
        CollapsiblePanel(const QString &text, const bool isExpanded, QWidget *parent = nullptr);

        void initialize();
        void setTitle(const QString text = "");
        void setExpanded(const bool value);
        void setPanelLayout(QLayout* layout);
        void collapseAll();
        void expandAll();

    signals:
        void collapsed();
        void expanded();
        void clicked();

    public slots:
        void slotToggleClicked();
        void slotToggleChanged();
        void slotShowContextMenu();

    public:
        QPushButton *button;
        QCheckBox *checkbox;
        QLabel *label;
        QGridLayout *layout;
        QWidget *widget;

        QMenu *contextMenu;
        QAction *collapseAct;
        QAction *expandAct;

    private:
        void createActions();
        void createMenus();
};

#endif // COLLAPSIBLEPANEL_H
