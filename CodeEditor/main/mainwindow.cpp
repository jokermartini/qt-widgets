#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QVBoxLayout>
#include <QLabel>
#include "codeeditor.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent)
{
    // controls
    auto *widget_main = new QWidget;
    auto *lay_main = new QVBoxLayout(widget_main);
    auto *label = new QLabel("Test");
    auto *editor = new CodeEditor();

    setCentralWidget(widget_main);

    // layout
    lay_main->addWidget(label);
    lay_main->addWidget(editor);
    lay_main->setContentsMargins(5, 5, 5, 5);
}

MainWindow::~MainWindow()
{
}
