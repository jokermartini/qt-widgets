#include "mainwindow.h"
#include <QDebug>
#include <QCoreApplication>
#include <QHostAddress>



//Commands:
//    "SMXS_SCRIPT",
//    "SMXS_FILE",
//    "SMXS_EXEC",        // Special command for Nexus
//    "SMXS_KILL",        // Kill Command for terminating the current listener

//    "VEXUS_SUBMIT",
//    "VEXUS_ECHO",        // Echo, not used for input

//    "VEXUS_NODES",
//    "VEXUS_LAYERS",
//    "VEXUS_CAMERAS",
//    "VEXUS_LIGHTS",
//    "VEXUS_MATERIALS",
//    "VEXUS_TEXTURES",
//    "VEXUS_MTL_SEL",
//    "VEXUS_MATLIB",

//    "VEXUS_RENDERERS",
//    "VEXUS_VEXUSMTL",
//    "VEXUS_QUICKREND",
//    "VEXUS_PRESETS",
//    "VEXUS_NODE_SETS",
//    "VEXUS_ATMOS"


MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
{
    m_button = new QPushButton("Send Message", this);
//    this->setCentralWidget(m_button);
    connect(m_button, SIGNAL(clicked(bool)), this, SLOT (slotSendMessage()));
}

MainWindow::~MainWindow()
{

}

void MainWindow::connectTcp()
{
    QHostAddress hostAddress;
    QByteArray data;
    quint16 port;

    hostAddress = QHostAddress::LocalHost;
    port = 7002;
    pSocket = new QTcpSocket( this );
    connect( pSocket, SIGNAL(readyRead()), SLOT(slotReadTcpData()) );

    pSocket->connectToHost(hostAddress, port);
    if( pSocket->waitForConnected() ) {
        // pSocket->write( data );

        QString cmd;
        cmd = QString("VEXUS_ECHO \"Test Text\"");
        qDebug() << cmd;
        qDebug() << cmd.toUtf8();
        data = cmd.toUtf8();

        pSocket->write( data );
        pSocket->close();

        // converting to w_char | +1 for terminating null
//        wchar_t *array = new wchar_t[cmd.length() + 1];
//        cmd.toWCharArray(array);
//        array[cmd.length()] = 0;

//        // Now 'array' is ready to use
//        pSocket->write( array );
//        pSocket->close();

//        // then delete in destructor
//        delete[] array;


//        char end = '\0';
//        pSocket->write(&end, sizeof(end));

//        qDebug() << "Sending Message:" << cmd;
//        AsciiPacket packet;
//        packet.setText(text);
//        write(packet.constData());
//        pSocket->write(cmd.toUtf8());

//        AsciiPacket packet;
//        packet.setText(text);
//        write(packet.constData());
//        char end = '\0';
//        write(&end, sizeof(end));

    }
}

void MainWindow::slotReadTcpData()
{
    QByteArray data = pSocket->readAll();
}

void MainWindow::slotSendMessage()
{
    this->connectTcp();
}
