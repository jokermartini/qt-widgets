#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTcpSocket>
#include <QPushButton>
#include <QVBoxLayout>

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = 0);
    ~MainWindow();

    void connectTcp();

    QTcpSocket *pSocket;
    QPushButton *m_button;

public slots:
    void slotReadTcpData();
    void slotSendMessage();

};

#endif // MAINWINDOW_H
