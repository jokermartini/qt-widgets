#include "mainwindow.h"
#include <QByteArray>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
{
    socket = new QTcpSocket(this);
    server = new QTcpServer(this);

    // signals/slots
    connect(socket, &QTcpSocket::connected, this, &MainWindow::slotConnected);
    connect(socket, &QTcpSocket::disconnected, this, &MainWindow::slotDisconnected);

    connect(server, &QTcpServer::newConnection, this, &MainWindow::slotNewConnection);

    // begin
    startListening();
    sendCommand();
}

MainWindow::~MainWindow()
{

}

void MainWindow::slotConnected()
{
    qDebug() << "Socket Connected!";
    // send intermediate command trying to get the name of 3ds max and print to the console
}

void MainWindow::slotDisconnected()
{
    qDebug() << "Socket Disconnected!";
}

void MainWindow::sendCommand()
{
    socket->connectToHost("127.0.0.1", 7002);

    if(socket->waitForConnected(3000))
    {
        // send
        socket->write("VEXUS_CAMERAS");
        socket->waitForBytesWritten(1000);
        socket->waitForReadyRead(3000);
//        qDebug() << "Reading: " << socket->bytesAvailable();
//        qDebug() << socket->readAll();

        socket->close();
        qDebug() << "Socket Closed!";
    }
    else
    {
        qDebug() << "Socket not connected!";
    }


//    QString echoMark("VEXUS_ECHO");
//    QString message("VEXUS_MATERIALS");
//    int index = message.lastIndexOf(echoMark);
//    while (index >= 0) {
//        QString content = message.right(message.length() - index - echoMark.length() + 1);
//        message = message.left(index);
//        index = message.lastIndexOf(echoMark);

//        emit server->commStateChanged(content);
//    }
//    return "";
//    QByteArray data; // <-- fill with data

//    _pSocket = new QTcpSocket( this ); // <-- needs to be a member variable: QTcpSocket * _pSocket;
//    connect( _pSocket, SIGNAL(readyRead()), SLOT(readTcpData()) );

//    _pSocket->connectToHost("127.0.0.1", 7002);
//    if( _pSocket->waitForConnected() ) {
//        _pSocket->write( data );
//    }
}

void MainWindow::readTcpData()
{
    QByteArray data = socket->readAll();
}

void MainWindow::startListening()
{
    if(!server->listen(QHostAddress::Any, 7011))
    {
        qDebug() << "Server could not start!";
    }
    else
    {
        qDebug() << "Server started!";
        qDebug() << socket->readAll();
    }
}

void MainWindow::slotNewConnection()
{
    qDebug() << "Server new connection!";
}
