#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTcpSocket>
#include <QTcpServer>

// https://www.bogotobogo.com/cplusplus/sockets_server_client_QT.php

class MainWindow : public QMainWindow
{
    Q_OBJECT

    public:
        MainWindow(QWidget *parent = 0);
        ~MainWindow();

        // Socket
        void sendCommand();
        void readTcpData();
        QTcpSocket *socket;

        // Server
        QTcpServer *server;
        void startListening();

    public slots:
        // Socket
        void slotConnected();
        void slotDisconnected();

        // Server
        void slotNewConnection();
};

#endif // MAINWINDOW_H
