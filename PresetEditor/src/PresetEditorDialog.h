#ifndef WIDGET_H
#define WIDGET_H

#include "presetitem.h"
#include "presetlistview.h"
#include "presetlistmodel.h"

#include <QDialog>
#include <QAction>
#include <QMenu>
#include <QMenuBar>
#include <QVBoxLayout>
#include <QListView>
#include <QDialogButtonBox>


class PresetEditorDialog : public QDialog
{
    Q_OBJECT

public:
    explicit PresetEditorDialog(QString filePath,
                                QWidget *parent = nullptr,
                                Qt::WindowFlags f = Qt::WindowFlags());
    ~PresetEditorDialog();

    // Controls
    PresetsView *listView;
    QAction *loadAct;
    QVBoxLayout *layout;
    QAction *saveAct;
    QAction *newPresetAct;
    QAction *editPresetAct;
    QAction *removePresetAct;
    QMenuBar *menuBar;
    QMenu *fileMenu;
    QMenu *contextMenu;
    QDialogButtonBox *buttonBox;

    void loadPresets(QString filepath="");
    void savePresets(QString filepath="");

private:
    QString filePath;
    PresetListModel * presetModel;

    //init gui
    void init();
    void loadJson(QString jsonPath);
    void saveJson(QString jsonPath);
    void createActions();
    void createMenus();

private slots:
    void slotContextMenu();
    void slotAddPreset();
    void slotEditPreset();
    void slotRemovePreset();

    void slotEditPresetFinish(PresetItem);
    void slotAddPresetFinish(PresetItem);

public slots:
    void accept() override;
};

#endif // WIDGET_H
