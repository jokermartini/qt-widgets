#ifndef PRESETLISTMODEL_H
#define PRESETLISTMODEL_H

#include <QObject>
#include <QAbstractListModel>
#include <QString>
#include <QList>
#include "presetitem.h"

class PresetListModel : public QAbstractListModel
{
    Q_OBJECT
public:
    enum Roles {
        PresetItemRole = Qt::UserRole + 1
    };
    PresetListModel(QObject *parent = nullptr);

    //override functions
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
    bool insertRows(int row, int count, const QModelIndex &parent = QModelIndex()) override;
    bool removeRows(int row, int count, const QModelIndex &parent = QModelIndex()) override;
    bool setData(const QModelIndex &index, const QVariant &value, int role = Qt::EditRole) override;

    bool setData(int row, const QVariant &value, int role = Qt::EditRole);
    QVariant data(int row, int role = Qt::DisplayRole) const;
    bool insertRow(int row, const QModelIndex &parent = QModelIndex());
    bool removeRow(int row, const QModelIndex &parent = QModelIndex());

    //insert PresetItem to list in way
    //that list will be in alphabet order
    bool insertDataAlphaOrder(PresetItem item);
    void clear();
private:
    QList<PresetItem> items;
};

#endif // PRESETLISTMODEL_H
