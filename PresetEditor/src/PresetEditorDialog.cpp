#include "PresetEditorDialog.h"
#include "dialogaddedit.h"

#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QFile>
#include <QDir>
#include <QFileDialog>
#include <QPushButton>


PresetEditorDialog::PresetEditorDialog(QString filePath,
                                       QWidget *parent,
                                       Qt::WindowFlags f) :
    QDialog(parent,f),
    filePath(filePath)
{ 
    init();
    loadJson(filePath);
}

void PresetEditorDialog::init()
{
    resize(400, 400);
    setWindowTitle("Presets");

    // controls
    listView = new PresetsView(this);
    listView->setContextMenuPolicy(Qt::CustomContextMenu);
    presetModel = new PresetListModel(this);
    listView->setModel(presetModel);

    buttonBox = new QDialogButtonBox(this);
    buttonBox->setOrientation(Qt::Horizontal);
    buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);

    // layout
    layout = new QVBoxLayout(this);
    layout->addWidget(listView);
    layout->addWidget(buttonBox);
    setLayout(layout);

    // menus/actions
    createActions();
    createMenus();

    // signals/slots
    connect(listView, &QListView::customContextMenuRequested, this, &PresetEditorDialog::slotContextMenu);
    connect(listView, &QListView::doubleClicked, this, &PresetEditorDialog::slotEditPreset);
    connect(buttonBox, SIGNAL(accepted()), this, SLOT(accept()));
    connect(buttonBox, SIGNAL(rejected()), this, SLOT(reject()));
}

void PresetEditorDialog::loadJson(QString jsonPath)
{
    QFile file(jsonPath);
    if(file.open(QIODevice::ReadOnly))
    {
        presetModel->clear();
        QByteArray ba = file.readAll();
        QJsonDocument document = QJsonDocument::fromJson(ba);
        auto array = document.array();
        for(int i=0; i<array.size(); i++)
        {
            auto object = array[i].toObject();
            PresetItem item;
            item.name = object.value("name").toString();
            item.post = object.value("post").toString();
            item.pre = object.value("pre").toString();
            item.submit = object.value("submit").toString();
            presetModel->insertDataAlphaOrder(item);
        }
        file.close();
    }
}

void PresetEditorDialog::saveJson(QString jsonPath)
{
    QFile file(jsonPath);
    if(file.open(QIODevice::WriteOnly))
    {
        QJsonArray array;
        for(int iR=0; iR<presetModel->rowCount(); iR++)
        {
            PresetItem item =
               qvariant_cast<PresetItem>(presetModel->data(iR, Qt::EditRole));
            QJsonObject obj;
            obj["name"]     = item.name;
            obj["post"]     = item.post;
            obj["pre"]      = item.pre;
            obj["submit"]   = item.submit;
            array.push_back(QJsonValue(obj));
        }
        QJsonDocument doc(array);
        auto data = doc.toJson();
        file.write(data);
        file.close();
    }
}

void PresetEditorDialog::createActions()
{
    loadAct = new QAction(tr("&Load..."), this);
    connect(loadAct, &QAction::triggered, this, [&](){loadPresets();});

    saveAct = new QAction(tr("&Save As..."), this);
    connect(saveAct, &QAction::triggered, this, [&](){savePresets();});

    newPresetAct = new QAction(tr("&New Preset..."), this);
    connect(newPresetAct, &QAction::triggered, this, PresetEditorDialog::slotAddPreset);

    editPresetAct = new QAction(tr("&Edit Preset"), this);
    connect(editPresetAct, &QAction::triggered, this, PresetEditorDialog::slotEditPreset);

    removePresetAct = new QAction(tr("&Remove Preset"), this);
    connect(removePresetAct, &QAction::triggered, this, PresetEditorDialog::slotRemovePreset);
}

void PresetEditorDialog::createMenus()
{
    fileMenu = new QMenu("File", this);
    fileMenu->addAction(newPresetAct);
    fileMenu->addSeparator();
    fileMenu->addAction(saveAct);
    fileMenu->addAction(loadAct);

    // Main menu bar
    menuBar = new QMenuBar(this);
    menuBar->addMenu(fileMenu);
    layout->setMenuBar(menuBar);

    // Context menu
    contextMenu = new QMenu();
    contextMenu->addAction(newPresetAct);
    contextMenu->addAction(editPresetAct);
    contextMenu->addSeparator();
    contextMenu->addAction(removePresetAct);
}

void PresetEditorDialog::accept()
{
    /*
    Desciption:
        Save the presets to a file rather than the qsettings
    */
    listView->closeCurrentEditor();
    savePresets(filePath);
    QDialog::accept();
}

void PresetEditorDialog::loadPresets(QString filepath)
{
    if (filepath.isEmpty())
        filepath = QFileDialog::getOpenFileName(this, "Open", "", "Vexus Settings Files (*.vxs)");

    loadJson(filepath);
}

void PresetEditorDialog::savePresets(QString filepath)
{
    if (filepath.isEmpty())
        filepath = QFileDialog::getSaveFileName(this, "Save", "", "Vexus Settings Files (*.vxs)");

    if (filepath.isEmpty())
        return;

    saveJson(filepath);
}

void PresetEditorDialog::slotContextMenu()
{
    if(listView->currentIndex().isValid()) {
        editPresetAct->setEnabled(true);
        removePresetAct->setEnabled(true);
    } else {
        editPresetAct->setEnabled(false);
        removePresetAct->setEnabled(false);
    }
    contextMenu->exec(QCursor::pos());
}

void PresetEditorDialog::slotAddPreset()
{
    DialogAddEdit dialog("New Preset Dialog");
    connect(&dialog, &DialogAddEdit::acceptedItem,
            this, &PresetEditorDialog::slotAddPresetFinish);
    dialog.exec();
}

void PresetEditorDialog::slotAddPresetFinish(PresetItem item)
{
    presetModel->insertDataAlphaOrder(item);
}

void PresetEditorDialog::slotEditPreset()
{
    auto itemData = qvariant_cast<PresetItem>(
                presetModel->data(listView->currentIndex(),Qt::EditRole));
    DialogAddEdit dialog("Edit Preset Dialog", itemData);
    connect(&dialog, &DialogAddEdit::acceptedItem,
            this, &PresetEditorDialog::slotEditPresetFinish);
    dialog.exec();
}

void PresetEditorDialog::slotEditPresetFinish(PresetItem item)
{
    int row = listView->currentIndex().row();
    if(item.name == presetModel->data(row, Qt::DisplayRole))
        presetModel->setData(row,
                             QVariant::fromValue(item));
    else
    {
        presetModel->removeRow(row);
        presetModel->insertDataAlphaOrder(item);
    }
}

void PresetEditorDialog::slotRemovePreset()
{
    int row = listView->currentIndex().row();
    presetModel->removeRow(row);
}

PresetEditorDialog::~PresetEditorDialog()
{
}
