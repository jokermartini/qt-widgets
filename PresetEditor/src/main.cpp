#include "PresetEditorDialog.h"
#include <QApplication>
#include <QStandardPaths>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    PresetEditorDialog  w(QStandardPaths::writableLocation(QStandardPaths::DocumentsLocation) + "/Vexus/Presets/submission_presets.vsp");
    w.show();

    return a.exec();
}
