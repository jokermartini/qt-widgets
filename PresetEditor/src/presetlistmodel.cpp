#include "presetlistmodel.h"

PresetListModel::PresetListModel(QObject *parent):
    QAbstractListModel(parent)
{
}

int PresetListModel::rowCount(const QModelIndex &parent) const
{
    return items.size();
}

QVariant PresetListModel::data(const QModelIndex &index, int role) const
{
    if(role == Qt::DisplayRole)
        return items[index.row()].name;
    else if(role == Qt::EditRole)
        return QVariant::fromValue(items[index.row()]);
    else
        return QVariant();
}

QVariant PresetListModel::data(int row, int role) const
{
    return data(index(row,0), role);
}

bool PresetListModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if(index.row() >= items.size() || index.row() < 0)
        return QAbstractListModel::setData(index,value,role);

    if(role == Qt::EditRole )
        items[index.row()] = qvariant_cast<PresetItem>(value);
    else if(role == Qt::DisplayRole)
        items[index.row()].name = value.toString();
    else
        return QAbstractListModel::setData(index,value,role);
    emit dataChanged(index, index, {role});
    return true;
}

void PresetListModel::clear()
{
    beginResetModel();
    items = {};
    endResetModel();
}

bool PresetListModel::insertDataAlphaOrder(PresetItem item)
{
    int insertPos = items.size();

    for(int i=0; i<items.size(); i++)
    {
        if(item.name < items[i].name)
        {
            insertPos = i;
            break;
        }
    }
    insertRow(insertPos);
    return setData(insertPos,QVariant::fromValue(item));
}

bool PresetListModel::setData(int row, const QVariant &value, int role)
{
    return setData(index(row,0), value, role);
}

bool PresetListModel::insertRows(int row, int count, const QModelIndex &parent)
{
    if(row < 0 || (row > items.size() && items.size()!= 0))
        return false;
    beginInsertRows(parent,row, row+count-1);
    for(int i=row; i<row+count; i++)
    {
        items.insert(i, PresetItem());
    }
    endInsertRows();
    return true;
}

bool PresetListModel::insertRow(int row, const QModelIndex &parent)
{
    return insertRows(row,1,parent);
}

bool PresetListModel::removeRow(int row, const QModelIndex &parent)
{
    return removeRows(row,1,parent);
}

bool PresetListModel::removeRows(int row, int count, const QModelIndex &parent)
{
    if(row < 0 || row+count > items.size())
        return false;
    beginRemoveRows(parent,row, row+count-1);
    for(int i=0; i<count; i++)
    {
        items.removeAt(row);
    }
    endRemoveRows();
    return true;
}
