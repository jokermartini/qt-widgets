#ifndef PRESETITEM_H
#define PRESETITEM_H

#include <QString>
#include <QMetaType>

struct PresetItem
{
    PresetItem(QString name,
               QString pre,
               QString submit,
               QString post):name(name),
        pre(pre), submit(submit), post(post)
    {}

    PresetItem():name(), pre(), submit(), post()
    {}

    QString name;
    QString pre;
    QString submit;
    QString post;
};
Q_DECLARE_METATYPE(PresetItem)

#endif // PRESETITEM_H
