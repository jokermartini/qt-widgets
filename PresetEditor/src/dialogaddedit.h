#ifndef DIALOGADDEDIT_H
#define DIALOGADDEDIT_H

#include <QDialog>
#include <QLineEdit>
#include <QDialogButtonBox>
#include "presetitem.h"


class DialogAddEdit : public QDialog
{
    Q_OBJECT

    public:
        explicit DialogAddEdit(QString title,
                               PresetItem itemEdit = PresetItem(), //need oly to edit dialog, to add dialog will be default
                               QWidget *parent = nullptr);
        ~DialogAddEdit();

        QLineEdit *lineEditName;
        QLineEdit *lineEditPre;
        QLineEdit *lineEditSubmit;
        QLineEdit *lineEditPost;
        QDialogButtonBox *buttonBox;

    private:
        //init gui
        void init(QString title, PresetItem itemEdit);
        void updateControls();

    signals:
        void acceptedItem(PresetItem);


    public slots:
        void accept() override;
};

#endif // DIALOGADDEDIT_H
