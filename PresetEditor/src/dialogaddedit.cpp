#include "dialogaddedit.h"
#include <QVBoxLayout>
#include <QFormLayout>
#include <QPushButton>


DialogAddEdit::DialogAddEdit(QString title, PresetItem itemEdit, QWidget *parent) :
    QDialog(parent)
{
    init(title, itemEdit);
}

void DialogAddEdit::init(QString title, PresetItem itemEdit)
{
    setWindowTitle(title);
    resize(400,100);

    // Controls
    QVBoxLayout *verticalLayout = new QVBoxLayout(this);
    QFormLayout *formLayout = new QFormLayout();

    lineEditName = new QLineEdit(this);
    lineEditPre = new QLineEdit(this);
    lineEditSubmit = new QLineEdit(this);
    lineEditPost = new QLineEdit(this);

    buttonBox = new QDialogButtonBox(this);
    buttonBox->setOrientation(Qt::Horizontal);
    buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);

    // Layout
    formLayout->addRow("Name", lineEditName);
    formLayout->addRow("Pre-Script", lineEditPre);
    formLayout->addRow("Submit-Script", lineEditSubmit);
    formLayout->addRow("Post-Script", lineEditPost);
    verticalLayout->addLayout(formLayout);
    verticalLayout->addStretch();
    verticalLayout->addWidget(buttonBox);

    // Populate data
    lineEditName->setText(itemEdit.name);
    lineEditPre->setText(itemEdit.pre);
    lineEditSubmit->setText(itemEdit.submit);
    lineEditPost->setText(itemEdit.post);

    //signals-slots connects
    connect(buttonBox, SIGNAL(accepted()), this, SLOT(accept()));
    connect(buttonBox, SIGNAL(rejected()), this, SLOT(reject()));
    connect(lineEditName, &QLineEdit::textChanged, this, &DialogAddEdit::updateControls);

    // start
    updateControls();
}

void DialogAddEdit::updateControls()
{
    //field Name is required
    if (lineEditName->text().trimmed().isEmpty()) {
        buttonBox->button(QDialogButtonBox::Ok)->setEnabled(false);
    } else {
        buttonBox->button(QDialogButtonBox::Ok)->setEnabled(true);
    }
}

void DialogAddEdit::accept()
{
    emit acceptedItem(PresetItem(lineEditName->text(),
                                 lineEditPre->text(),
                                 lineEditSubmit->text(),
                                 lineEditPost->text()));
    QDialog::accept();
}

DialogAddEdit::~DialogAddEdit()
{
}

