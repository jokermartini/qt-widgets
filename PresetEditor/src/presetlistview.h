#ifndef HOTKEYLISTVIEW_H
#define HOTKEYLISTVIEW_H

#include <QListView>

class PresetsView : public QListView
{
    Q_OBJECT
public:
    explicit PresetsView(QWidget *parent = nullptr);
    void closeCurrentEditor();
};

#endif // HOTKEYLISTVIEW_H
