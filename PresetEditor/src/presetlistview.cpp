#include "presetlistview.h"

PresetsView::PresetsView(QWidget *parent) :
    QListView(parent)
{
}

void PresetsView::closeCurrentEditor()
{
    auto index = currentIndex();
    if (index.isValid())
    {
        auto ix = index.sibling(index.row(), 1);
        auto widget = indexWidget(ix);
        if (widget)
        {
            commitData(widget);
            closeEditor(widget, QAbstractItemDelegate::NoHint);
        }
    }
}

