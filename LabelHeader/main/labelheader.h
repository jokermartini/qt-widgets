#ifndef LABELHEADER_H
#define LABELHEADER_H

#include <QLabel>

class LabelHeader : public QLabel
{
    Q_OBJECT
    Q_PROPERTY(PatternType pattern READ pattern WRITE setPattern)

public:
    enum PatternType {
        NoPattern,
        DashedDotsPattern,
        SolidLinePattern
    };

    explicit LabelHeader(QWidget *parent = nullptr, Qt::WindowFlags f = Qt::WindowFlags());
    explicit LabelHeader(const QString &text, QWidget *parent = nullptr, Qt::WindowFlags f = Qt::WindowFlags());

    PatternType pattern() const;
    void setPattern(const PatternType &pattern);

private:
    PatternType m_pattern;

protected:
    void paintEvent(QPaintEvent *event);
};

#endif // LABELHEADER_H
