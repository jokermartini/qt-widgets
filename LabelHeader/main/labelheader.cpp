#include "labelheader.h"
#include <QPainter>

LabelHeader::LabelHeader(QWidget *parent, Qt::WindowFlags f) :
    LabelHeader("", parent, f)
{

}

LabelHeader::LabelHeader(const QString &text, QWidget *parent, Qt::WindowFlags f) :
    QLabel(text, parent, f),
    m_pattern(NoPattern)
{

}

LabelHeader::PatternType LabelHeader::pattern() const
{
    return m_pattern;
}

void LabelHeader::setPattern(const PatternType &pattern)
{
    if (m_pattern == pattern)
        return;

    m_pattern = pattern;
    update();
}

void LabelHeader::paintEvent(QPaintEvent *event)
{
    // calculate font width
    QFontMetrics metrics(font());
    int text_width = metrics.boundingRect(text()).width();

    // calculate dimensions
    int y = height() * 0.5;
    int x = text_width + 4;

    // create pattern
    QPixmap px(4, 4);
    px.fill(Qt::transparent);
    QPainter pattern_painter(&px);

    // draw
    QPainter painter(this);

    switch( pattern() ) {
        case LabelHeader::NoPattern:
            break;
        case LabelHeader::DashedDotsPattern:
            // Create dashed 3 dots
            pattern_painter.setPen(Qt::NoPen);
            pattern_painter.setBrush(QBrush(palette().color(QPalette::WindowText), Qt::SolidPattern));
            pattern_painter.drawRect(0, 0, 1, 1);
            pattern_painter.drawRect(2, 2, 1, 1);
            painter.drawTiledPixmap(x, y-2, width()-x, 5, px);
            break;
        case LabelHeader::SolidLinePattern:
            painter.drawLine(x,y,width(),y);
            break;
    }

    QLabel::paintEvent(event);
}
