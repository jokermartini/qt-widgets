#include "mainwindow.h"
#include <QVBoxLayout>
#include <QComboBox>
#include "labelheader.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
{
    // Controls
    auto *widget_main = new QWidget();
    auto *layout = new QVBoxLayout(widget_main);
    auto *label = new LabelHeader(tr("General"), this);
    auto *list = new QComboBox(this);

    //label->setPattern(LabelHeader::DashedDotsPattern);
    //label->setStyleSheet("QLabel { color: rgb(255,0,0); }");
    list->addItems(QStringList({"Pattern 1", "Pattern 2", "Pattern 3"}));

    // Layout
    layout->addWidget(label);
    layout->addWidget(list);
    layout->addStretch();
    widget_main->setLayout(layout);

    setCentralWidget(widget_main);
    resize(200,300);

    // Connections
    connect(list, static_cast<void (QComboBox::*)(int)>(&QComboBox::currentIndexChanged), [label](int index){
            label->setPattern(static_cast<LabelHeader::PatternType>(index));
        });
}

MainWindow::~MainWindow()
{

}
